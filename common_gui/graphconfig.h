#ifndef GRAPHCONFIG_H
#define GRAPHCONFIG_H

#include <QDialog>
#include <QFontDialog>
#include <QColorDialog>
#include <QPen>
#include <QBrush>

const int GRAPH_ALIGN_TOP    = 0;
const int GRAPH_ALIGN_BOTTOM = 1;
const int GRAPH_ALIGN_LEFT   = 2;
const int GRAPH_ALIGN_RIGHT  = 3;
const int GRAPH_ALIGN_CUSTOM = 4;

namespace Ui {
class graphConfig;
}

class graphConfig : public QDialog
{
    Q_OBJECT

public:
    explicit graphConfig(QWidget *parent = 0);
    ~graphConfig();

    // Check enable components
    void checkEnable();

    // Title setters and getters
    QString getTitleText() const;
    void setTitleText(const QString &value);
    QFont getTitleFont() const;
    void setTitleFont(const QFont &value);
    QBrush getTitleBrush() const;
    void setTitleBrush(const QBrush &value);

    // Style
    void deleteStyleSheet();

    // Leggend setters and getters
    bool   getLegedVisible();
    void   setLegendVisible(bool v);
    bool   getBackgroundVisible();
    void   setBackgroundVisible(bool v);
    QColor getLabelFontColor();
    void   setLabelFontColor(QColor c);
    void   setBackgroundPen(QPen p);
    QPen   getBackgroundPen();
    void   setLegendFont(QFont f);
    QFont  getLegendFont();
    void   setBackgroundBrush(QBrush b);
    QBrush getBackgroundBrush();
    void   setX(int x1, int x2, int xMax);
    void   setY(int y1, int y2, int yMax);
    int    getX1();
    int    getY1();
    int    getX2();
    int    getY2();
    void   setAlignment(Qt::Alignment a);
    void   setAttached(bool a);
    Qt::Alignment getAlignment();
    bool  getAttached();

    // Axis
    QString getXcaption();
    QString getYcaption();

private slots:
    void on_lTitleGraph_textChanged(const QString &arg1);
    void on_tbTitleFont_clicked();
    void on_tbTitleColor_clicked();
    void on_tbLabelFontColor_clicked();
    void on_tbBorder_clicked();
    void on_tbLabelFont_clicked();
    void on_tbBackground_clicked();

    void on_chkLegendVisible_stateChanged(int arg1);

    void on_cbAlign_currentIndexChanged(int index);

private:
    Ui::graphConfig *ui;
    QFontDialog  dFont;
    QColorDialog dColor;

    // Title info
    QString titleText;
    QFont   titleFont;
    QBrush  titleBrush;

    // Legend
    QColor cLabelFontColor;
    QPen   pBorder;
    QBrush bBackground;
    QFont  fLegend;
};

#endif // GRAPHCONFIG_H
