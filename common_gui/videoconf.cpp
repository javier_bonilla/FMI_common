/*
*    This file is part of simint
*
*    simint - Simulation Interface
*    Copyright (C) 2017 - Javier Bonilla
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "videoconf.h"
#include "ui_videoconf.h"
#include "commonapp.h"

#include <QPushButton>
#include <QFileDialog>

VideoConf::VideoConf(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::VideoConf)
{
    ui->setupUi(this);
}

VideoConf::~VideoConf()
{
    delete ui;
}

void VideoConf::setData(double time, int frames, int fps)
{
    t = time;
    m = 1;
    f = frames;
    ui->lbTime->setText(QString::number(time));
    ui->lbFrames->setText(QString::number(frames));
    ui->sbFPS->setValue(fps);
    setState();
}

void VideoConf::on_sbFPS_valueChanged(const QString &arg1)
{
    Q_UNUSED(arg1);

    setState();
}

int VideoConf::getFPS()
{
    return ui->sbFPS->value();
}

void VideoConf::setFPS(int value)
{
    ui->sbFPS->setValue(value);
    setState();
}

double VideoConf::getMultiplier()
{
    return m;
}

void VideoConf::setMultiplier(double value)
{
    m = value;

    if (value==1)
        ui->rbReal->setChecked(true);
    else if (value>1)
    {
        ui->rbSlow->setChecked(true);
        ui->sbSlow->setValue(value);
        ui->sbFast->setValue(2);
        setState();
    }
    else
    {
        ui->rbFast->setChecked(true);
        ui->sbFast->setValue(1/value);
        ui->sbSlow->setValue(2);
        setState();
    }
}

void VideoConf::on_buttonBox_clicked(QAbstractButton *button)
{
    if (button == ui->buttonBox->button(QDialogButtonBox::Save))
    {
        QFileDialog dlg(this, mVideoFile, "video", AVIFilter);

        dlg.setAcceptMode(QFileDialog::AcceptSave);
        dlg.setOption(QFileDialog::DontConfirmOverwrite,false);

        if (dlg.exec())
        {
            filename = dlg.selectedFiles()[0];
            accept();
        }
    }
    else if (button == ui->buttonBox->button(QDialogButtonBox::Ok))
        accept();
    else if (button == ui->buttonBox->button(QDialogButtonBox::Cancel))
        reject();
}

void VideoConf::setState()
{
    const QString sColorBlue = "color: blue;";
    const QString sColorRed  = "Color: red;";
    double totalFrames = getTotalFrames();

    ui->sbFast->setEnabled(ui->rbFast->isChecked());
    ui->sbSlow->setEnabled(ui->rbSlow->isChecked());
    ui->lbVideoTime->setText(QString::number(t*m));
    ui->lbVideoFrames->setText(QString::number(totalFrames));
    ui->buttonBox->button(getSaveToFile() ? QDialogButtonBox::Save : QDialogButtonBox::Ok)->setEnabled(totalFrames>=1 && totalFrames<=f);
    ui->lbVideoFrames->setStyleSheet(validState() ? sColorBlue : sColorRed);
}

double VideoConf::getTotalFrames()
{
    return ui->sbFPS->value()*t*m;
}

bool VideoConf::validState()
{
    double totalFrames = getTotalFrames();

    return totalFrames>=1 && totalFrames<=f;
}

void VideoConf::on_rbReal_clicked()
{
    m = 1;
    setState();
}

void VideoConf::on_rbSlow_clicked()
{
    on_sbSlow_valueChanged("");
}

void VideoConf::on_rbFast_clicked()
{
    on_sbFast_valueChanged("");
}

void VideoConf::on_sbSlow_valueChanged(const QString &arg1)
{
    Q_UNUSED(arg1);

    m = ui->sbSlow->value();
    setState();
}

void VideoConf::on_sbFast_valueChanged(const QString &arg1)
{
    Q_UNUSED(arg1);

    m = 1.0/ui->sbFast->value();
    setState();
}

bool VideoConf::getSaveToFile() const
{
    return saveToFile;
}

void VideoConf::setSaveToFile(bool value)
{
    saveToFile = value;
    if (getSaveToFile())
        ui->buttonBox->setStandardButtons(QDialogButtonBox::Save | QDialogButtonBox::Cancel);
    else
        ui->buttonBox->setStandardButtons(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
}

QString VideoConf::getFilename() const
{
    return filename;
}
