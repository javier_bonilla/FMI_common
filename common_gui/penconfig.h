#ifndef PENCONFIG_H
#define PENCONFIG_H

#include <QDialog>
#include <QPen>
#include <QColorDialog>

namespace Ui {
class penConfig;
}

class penConfig : public QDialog
{
    Q_OBJECT

public:
    explicit penConfig(QWidget *parent = 0);
    ~penConfig();
    void setPen(QPen p);
    QPen getPen();
    void deleteSytyleSheet();

private slots:
    void on_sbWidth_valueChanged(int arg1);
    void on_tbColor_clicked();
    void on_cbLine_currentIndexChanged(int index);

private:
    Ui::penConfig *ui;
    QPen pen;
    QColorDialog dColor;
};

#endif // PENCONFIG_H
