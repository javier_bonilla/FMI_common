#include "brushconf.h"
#include "ui_brushconf.h"

#include "commonapp.h"

brushConf::brushConf(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::brushConf)
{
    ui->setupUi(this);
}

brushConf::~brushConf()
{
    delete ui;
}

void brushConf::setBrush(QBrush b)
{
    brush = b;
    brushToInterface();
}

void brushConf::deleteSytyleSheet()
{
    ui->lTitle->setStyleSheet(sEmpty);
}

QBrush brushConf::getBrush()
{
    return brush;
}

void brushConf::brushToInterface()
{
    // Radio buttons
    ui->rbGradient->setChecked(brush.style() == Qt::LinearGradientPattern ||
                               brush.style() == Qt::RadialGradientPattern ||
                               brush.style() == Qt::ConicalGradientPattern);
    ui->rbTexture->setChecked(brush.style()  == Qt::TexturePattern);
    ui->rbColor->setChecked(!ui->rbGradient->isChecked() && !ui->rbTexture->isChecked());

    // Color & style
    setBrushStyleCombo(ui->cbStyle, brush);
    setBackgroundColor(ui->leColor ,brush.color());

    // Gradient
    //setGradientStyleCombo(ui->cbStyle, brush);
    setBackgroundColor(ui->leColor1,brush.color());
    //setBackgroundColor(ui->leColor2,brush.gradient());

    // Texture

}

void brushConf::interfaceToBrush()
{

}

void brushConf::checkEnable()
{
    ui->cbStyle->setEnabled(ui->rbColor->isChecked());
    ui->tbColor->setEnabled(ui->rbColor->isChecked());
    ui->cbGradient->setEnabled(ui->rbGradient->isChecked());
    ui->tbColor1->setEnabled(ui->rbGradient->isChecked());
    ui->tbColor2->setEnabled(ui->rbGradient->isChecked());
    ui->tbTexture->setEnabled(ui->rbTexture->isChecked());
}

void brushConf::on_tbColor_clicked()
{
    dColor.setCurrentColor(brush.color());
    dColor.setWindowIcon(windowIcon());
    if (dColor.exec())
    {
        brush.setColor(dColor.currentColor());
        setBrushStyleCombo(ui->cbStyle,brush);
        setBackgroundColor(ui->leColor ,brush.color());
    }
}

void brushConf::on_rbColor_clicked()
{
    checkEnable();
}

void brushConf::on_rbGradient_clicked()
{
    checkEnable();
}

void brushConf::on_rbTexture_clicked()
{
    checkEnable();
}

void brushConf::on_cbStyle_currentIndexChanged(int index)
{
    brush.setStyle((Qt::BrushStyle)index);
}
