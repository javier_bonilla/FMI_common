#include "commonapp.h"
#include <QFileInfo>
#include <QPainter>
#include <QPrinter>
#include <QtSvg/QSvgGenerator>
#include <QClipboard>
#include <QApplication>
#include <QScreen>
#include <QSpinBox>
#include <QValueAxis>
#include <QDateTimeAxis>
#include <QFileDialog>
#include <QTextEdit>
#include <QMenu>
#include <QGraphicsProxyWidget>
#include <QMessageBox>
#include <QPieSlice>
#include <QPieSeries>
#include <QBarSet>
#include <QBarCategoryAxis>

#include "formats/read_csv.h"
#include "formats/read_matlab4.h"
//#include "confapp.h"

const int VAR_POS     = 0;
const int VAR_VALUE   = 1;

const double DEF_AXIS_X_MIN = 0;
const double DEF_AXIS_X_MAX = 1;
const double DEF_AXIS_Y_MIN = 0;
const double DEF_AXIS_Y_MAX = 1;

// Hint in series
const QString cHintStyle = "background-color: %1;";
const int     POINT_SIZE = 6;

// HTML regular expression for tags
const QString HTML_EXP = "<(.)[^>]*>";

// -------
// WARNING:
// -------
// Global objects!. This must be changed.

// Selected series
QMutex                mutex;
bool                  serie = false;
QGraphicsEllipseItem *qge   = NULL;
QTextEdit            *txt   = NULL;

QString escapeValue(QString value, QString separator, QString escape)
{
    if (value.indexOf(escape)>=0)
    {
        value.replace(escape,escape+escape);
        value = escape + value + escape;
    }
    if (value.indexOf(separator)>=0)
        value = escape + value + escape;

    return value;
}

QString copyVars(QList<double> time, QList<QList<double>> vars, QList<QString> names, QString colSeparator, QString rowSeparator)
{
    const QString escape = "\"";
    QString str = sEmpty;

    if (names.count()>0)
    {
        // Header
        for(int i=0;i<names.count();i++)
        {
            str += escapeValue(names[i],colSeparator,escape);
            str += colSeparator;
        }
        str = str.mid(0,str.count()-1) + rowSeparator;

        // Data
        for(int i=0;i<time.count();i++)
        {
            str += escapeValue(toLocalDecimalSeparator(QString::number(time[i])),colSeparator,escape);
            str += colSeparator;

            for(int j=0;j<vars.count();j++)
            {
                str += escapeValue(toLocalDecimalSeparator(QString::number(vars[j][i])),colSeparator,escape);
                str += colSeparator;
            }
            str = str.mid(0,str.count()-1) + rowSeparator;
        }
        str = str.mid(0,str.count()-1);
    }
    return str;
}

QList<double> serieXlist(QList<QPointF> p)
{
    QList<double> r;

    for(int i=0;i<p.count();i++)
        r.append(p[i].x());

    return r;
}

void chartToData(QChartView *qcv, QList<double> &time, QList<QList<double>> &vars, QList<QString> &names)
{
    QLineSeries   *sLine = NULL;
    QAreaSeries   *sArea = NULL;
    QPieSeries    *sPie  = NULL;
    QBarSeries    *sBar  = NULL;
    QList<QPointF> points;
    QString        axis;
    QList<double> v;

    time.clear();
    vars.clear();
    names.clear();

    // Select time series with more values
    for (int i=0;i<qcv->chart()->series().count();i++)
    {

        sLine = qobject_cast<QLineSeries *>(qcv->chart()->series()[i]);
        sArea = qobject_cast<QAreaSeries *>(qcv->chart()->series()[i]);

        if (sLine!=NULL) points = sLine->points();
        if (sArea!=NULL) points = sArea->upperSeries()->points();

        time = points.count()>time.count() ? serieXlist(points) : time;
    }

    // Save all series from previous time series
    for (int i=0;i<qcv->chart()->series().count();i++)
    {
        QString name;

        sLine = qobject_cast<QLineSeries *>(qcv->chart()->series()[i]);
        sArea = qobject_cast<QAreaSeries *>(qcv->chart()->series()[i]);
        sPie  = qobject_cast<QPieSeries  *>(qcv->chart()->series()[i]);
        sBar  = qobject_cast<QBarSeries  *>(qcv->chart()->series()[i]);

        if (sLine!=NULL){name = sLine->name(); axis = axisName(sLine->attachedAxes(),Qt::AlignBottom); points = sLine->points();}
        if (sArea!=NULL){name = sArea->name(); axis = axisName(sArea->attachedAxes(),Qt::AlignBottom); points = sArea->upperSeries()->points();}

        if (sLine!=NULL || sArea!=NULL)
        {
            serieToDataForTimeInt(points,time,v);
            vars.append(v);
            if (names.isEmpty()) names.append(axis); else names.replace(0,axis);
            names.append(name);
        }
        else if (sPie!=NULL)
        {
            names.clear();
            v.clear();
            for(int j=0;j<sPie->count();j++)
            {
                names.append(sPie->property(VAR_NAME.toStdString().c_str()).toString());
                v.append(sPie->slices()[j]->value());
            }
            time.append(qcv->property(VAR_TIME_VAL.toStdString().c_str()).toDouble());
            vars.append(v);
            if (names.count()>0) names.insert(0,axis);
        }
        else if (sBar!=NULL && !sBar->barSets().isEmpty())
        {
            // Names
            names.clear();
            names.append(sEmpty);
            for(int j=0;j<sBar->barSets().count();j++)
                names.append(sBar->barSets()[j]->label());
            // Values
            for(int j=0;j<sBar->barSets().count();j++)
            {
                v.clear();
                for(int k=0;k<sBar->barSets()[j]->count();k++)
                        v.append(sBar->barSets()[j]->at(k));
                vars.append(v);
            }
            // Time - x axis
            time.clear();
            QList<QAbstractAxis*> axes=sBar->attachedAxes();
            for(int j=0;j<axes.count();j++)
            {
                QBarCategoryAxis *bca = qobject_cast<QBarCategoryAxis *>(axes[j]);
                if(bca!=NULL)
                {
                    names.replace(0,bca->titleText());
                    for(int k=0;k<bca->count();k++)
                    {
                        bool   ok;
                        double val = bca->at(k).toDouble(&ok);

                        time.append(ok ? val : k);
                    }
                }
            }
        }
    }
}

QString axisName(QList<QAbstractAxis*> axis, Qt::Alignment a)
{
    for(int i=0;i<axis.count();i++)
    {
        if (axis[i]->alignment() == a)
            return axis[i]->titleText();
    }
    return "";
}

void serieToData(QLineSeries *serie, QList<double> &time, QList<double> &values, QString &name)
{
    time.clear();
    values.clear();   
    name = serie->name();

    QList<QPointF> points = serie->points();
    for(int i=0;i<points.count();i++)
    {
        time.append(points[i].x());
        values.append(points[i].y());
    }
}

void serieToDataForTimeInt(QList<QPointF> points, QList<double> time, QList<double> &values)
{
    values.clear();

    QList<double>  sTime  = serieXlist(points);
    for(int i=0;i<time.count();i++)
    {
        int index = sTime.indexOf(time[i]);
        values.append(index>=0 ? points[index].y() : NAN);
    }
}

void copyVarsClipboard(QList<double> time, QList<QList<double>> vars, QList<QString> names)
{
    QString str = copyVars(time,vars,names,tab,newLine);
    QApplication::clipboard()->setText(str);
}

void copyVarsFile(QWidget *parent, QList<double> time, QList<QList<double>> vars, QList<QString> names)
{
    QFileDialog dlg(parent, mTextFile, sEmpty, CSVFilter);

    dlg.setAcceptMode(QFileDialog::AcceptSave);
    dlg.setOption(QFileDialog::DontConfirmOverwrite,false);

    if (dlg.exec())
    {
        QString str = copyVars(time,vars,names,comma,newLine);
        QFile   file(dlg.selectedFiles()[0]);

        if(file.open(QIODevice::WriteOnly|QIODevice::Truncate|QIODevice::Text))
        {
            QTextStream stream(&file);
            stream << str;
        }
        file.close();
    }
}

void chartToImageFile(QWidget *parent, QChartView *qcv)
{    
    QFileDialog dlg(parent, mImagFile, sEmpty, PDFFilter + ";;" + SVGFilter + ";;" + PNGFilter + ";;" + BMPFilter + ";;" + JPGFilter + ";;" + JPEGFilter);

    dlg.setAcceptMode(QFileDialog::AcceptSave);
    dlg.setOption(QFileDialog::DontConfirmOverwrite,false);

    if (dlg.exec())
    {
        if (dlg.selectedNameFilter() == PDFFilter)       chartToPDF(qcv,dlg.selectedFiles()[0]);
        else if (dlg.selectedNameFilter() == SVGFilter)  chartToSVG(qcv,dlg.selectedFiles()[0]);
        else                                             chartToImage(qcv,false,dlg.selectedFiles()[0]);
    }
}

void imageToFile(QWidget *parent, QImage image)
{
    QFileDialog dlg(parent, mImagFile, sEmpty, PDFFilter + ";;" + SVGFilter + ";;" + PNGFilter + ";;" + BMPFilter + ";;" + JPGFilter + ";;" + JPEGFilter);

    dlg.setAcceptMode(QFileDialog::AcceptSave);
    dlg.setOption(QFileDialog::DontConfirmOverwrite,false);

    if (dlg.exec())
    {
        if (dlg.selectedNameFilter() == PDFFilter)       imageToPDF(image,dlg.selectedFiles()[0]);
        else if (dlg.selectedNameFilter() == SVGFilter)  imageToSVG(image,dlg.selectedFiles()[0]);
        else                                             imageToImg(image,false,dlg.selectedFiles()[0]);
    }
}

void imageToPDF(QImage image, QString file)
{
    QPainter *painter = new QPainter();
    QPrinter *printer = new QPrinter();

    printer->setOutputFileName(file);
    printer->setOrientation(QPrinter::Landscape);
    printer->setPaperSize(QSizeF(image.heightMM(),image.widthMM()),QPrinter::Millimeter);
    printer->setFontEmbeddingEnabled(true);
    printer->setCreator("Optifmus PDF file");
    printer->setDocName("Parameter estimation results from Optifmus");
    printer->setOutputFormat(QPrinter::PdfFormat);
    painter->begin(printer);
    painter->drawImage(0,0,image);
    painter->end();

    delete(painter);
    delete(printer);
}

//void chartToPDF(QAbstract3DGraph *qcv, QString file)
//{
//    imageToPDF(qcv->renderToImage(),file);
//}

void imageToSVG(QImage image, QString file)
{
    QPainter      *painter   = new QPainter();
    QSvgGenerator *generator = new QSvgGenerator();

    generator->setFileName(file);
    generator->setSize(image.size());
    generator->setViewBox(image.rect());
    generator->setTitle("Optifmus SVG file");
    generator->setDescription("Parameter estimation results from Optifmus");
    painter->begin(generator);
    painter->drawImage(0,0,image);
    painter->end();

    delete(painter);
    delete(generator);
}

//void chartToSVG(QAbstract3DGraph *qcv, QString file)
//{
//    imageToSVG(qcv->renderToImage(),file);
//}

void imageToImg(QImage image, bool clipboard, QString file)
{
    if (clipboard)
        QApplication::clipboard()->setImage(image);
    else
        image.save(file);
}

//void chartToImage(QAbstract3DGraph *qcv, bool clipboard, QString file)
//{
//    QImage image = qcv->renderToImage();

//    if (clipboard)
//        QApplication::clipboard()->setImage(image);
//    else
//        image.save(file);
//}


void chartToPDF(QGraphicsView *qcv, QString file)
{
    QPainter *painter = new QPainter();
    QPrinter *printer = new QPrinter();

    printer->setOutputFileName(file);
    printer->setOrientation(QPrinter::Landscape);
    printer->setPaperSize(QSize(qcv->heightMM(), qcv->widthMM()),QPrinter::Millimeter);
    printer->setFontEmbeddingEnabled(true);
    printer->setCreator("Optifmus PDF file");
    printer->setDocName("Parameter estimation results from Optifmus");
    printer->setOutputFormat(QPrinter::PdfFormat);
    painter->begin(printer);
    qcv->render(painter);
    painter->end();

    delete(painter);
    delete(printer);
}

void chartToSVG(QGraphicsView *qcv, QString file)
{
    QPainter      *painter   = new QPainter();
    QSvgGenerator *generator = new QSvgGenerator();

    generator->setFileName(file);

    generator->setSize(qcv->size());
    generator->setViewBox(qcv->rect());
    generator->setTitle("Optifmus SVG file");
    generator->setDescription("Parameter estimation results from Optifmus");
    painter->begin(generator);
    qcv->render(painter);
    painter->end();

    delete(painter);
    delete(generator);
}

void chartToImage(QGraphicsView *qcv, bool clipboard, QString file)
{
    QPainter *painter = new QPainter();
    QImage   *image   = new QImage(qcv->size(),QImage::Format_ARGB32_Premultiplied);

    painter->begin(image);
    qcv->render(painter);
    painter->end();
    if (clipboard)
        QApplication::clipboard()->setImage(*image);
    else
        image->save(file);
    delete(painter);
    delete(image);
}

void filterTree(QTreeWidget *tree, int textPos, QString cad, bool caseSensitive)
{
    QTreeWidgetItemIterator it(tree);
    QRegExp reg;

    if (cad.isEmpty()) cad = "*";
    reg.setPatternSyntax(QRegExp::Wildcard);
    reg.setCaseSensitivity(caseSensitive ? Qt::CaseSensitive : Qt::CaseInsensitive);
    reg.setPattern(cad);
    while (*it) {
        if (reg.exactMatch((*it)->text(textPos)))
        {
            (*it)->setHidden(false);
            QTreeWidgetItem *item = (*it)->parent();

            while (item != NULL)
            {
                item->setHidden(false);
                item = item->parent();
            }
        }
        else
            (*it)->setHidden(true);
        ++it;
    }
}

QString integratorToText(IntegratorType it)
{
    switch (it) {
        case IntegratorType::eu:   return "Forward Euler method - ODEINT"; break;
        case IntegratorType::rk:   return "4th order Runge-Kutta method - ODEINT"; break;
        case IntegratorType::abm:  return "Adams-Bashforth-Moulton method - ODEINT"; break;
        case IntegratorType::ck:   return "5th order Runge-Kutta-Cash-Karp method - ODEINT"; break;
        case IntegratorType::dp:   return "5th order Runge-Kutta-Dormand-Prince method - ODEINT"; break;
        case IntegratorType::fe:   return "8th order Runge-Kutta-Fehlberg method - ODEINT"; break;
        case IntegratorType::bs:   return "Bulirsch-Stoer method - ODEINT"; break;
        case IntegratorType::ro:   return "4th Rosenbrock method - ODEINT"; break;
        case IntegratorType::bdf:  return "Backwards Differentiation Formula (BDF) - SUNDIALS"; break;
        case IntegratorType::abm2: return "Adams-Bashforth-Moulton method - SUNDIALS"; break;
        default: return "Not found"; break;
    }
}

void writeState(QString company, QString appName, QHeaderView *qw, QString id)
{
    QSettings settings(company,appName);
    settings.setValue(id+pSep+pState,qw->saveState());
}

void writeState(QString company, QString appName, QSplitter *s, QString id)
{
    QSettings settings(company,appName);
    settings.setValue(id+pSep+pState,s->saveState());
}

void writeGeometry(QString company, QString appName, QMainWindow *qw, QString id)
{
    QSettings settings(company,appName);
    settings.setValue(id+pSep+pGeometry,qw->saveGeometry());
    settings.setValue(id+pSep+pState,qw->saveState());
}

void readGeometry(QString company, QString appName, QMainWindow *qw, QString id)
{
    QSettings settings(company,appName);

    if (settings.contains(id+pSep+pGeometry))
        qw->restoreGeometry(settings.value(id+pSep+pGeometry).toByteArray());
    if (settings.contains(id+pSep+pState))
        qw->restoreState(settings.value(id+pSep+pState).toByteArray());
}

void writeGeometry(QString company, QString appName, QDialog *qw, QString id)
{
    QSettings settings(company,appName);
    settings.setValue(id+pSep+pGeometry,qw->saveGeometry());
}

void readGeometry(QString company, QString appName, QDialog *qw, QString id)
{
    QSettings settings(company,appName);

    if (settings.contains(id+pSep+pGeometry))
        qw->restoreGeometry(settings.value(id+pSep+pGeometry).toByteArray());
}

void readGeometry(QString company, QString appName, QTableWidget *table, QString id)
{
    QSettings settings(company,appName);
    table->horizontalHeader()->restoreGeometry(settings.value(id+pSep+pGeometry).toByteArray());
}

void writeGeometry(QString company, QString appName, QTableWidget *table, QString id)
{
    QSettings settings(company,appName);
    settings.setValue(id+pSep+pGeometry,table->horizontalHeader()->saveGeometry());
}

void readState(QString company, QString appName, QTableWidget *table, QString id)
{
    QSettings settings(company,appName);
    table->horizontalHeader()->restoreState(settings.value(id+pSep+pGeometry).toByteArray());
}

void writeState(QString company, QString appName, QTableWidget *table, QString id)
{
    QSettings settings(company,appName);
    settings.setValue(id+pSep+pGeometry,table->horizontalHeader()->saveState());
}

void writeOption(QString company, QString appName, QString option, QString value)
{
    QSettings settings(company,appName);
    settings.setValue(option,value);
}

void writeOption(QString company, QString appName, QString option, bool value)
{
    QSettings settings(company,appName);
    settings.setValue(option,value);
}

void writeOption(QString company, QString appName, QString option, int value)
{
    QSettings settings(company,appName);
    settings.setValue(option,value);
}

QString readOption(QString company, QString appName, QString option, QString defValue)
{
    QSettings settings(company,appName);
    return settings.value(option,defValue).toString();
}

bool readOption(QString company, QString appName, QString option, bool defValue)
{
    QSettings settings(company,appName);
    return settings.value(option,defValue).toBool();
}

int readOption(QString company, QString appName, QString option, int defValue)
{
    QSettings settings(company,appName);
    return settings.value(option,defValue).toInt();
}


void readState(QString company, QString appName, QHeaderView *qw, QString id)
{
    QSettings settings(company,appName);
    qw->restoreState(settings.value(id+pSep+pState).toByteArray());
}

void readState(QString company, QString appName, QSplitter *s, QString id)
{
    QSettings settings(company,appName);
    s->restoreState(settings.value(id+pSep+pState).toByteArray());
}

unsigned readVarsCSVfile(QString file, QList<variable> &vars, QList<double> &first, QList<double> &last, int &num_mea, QString &error)
{
    struct csv_data *csv_reader;

    error        = sEmpty;
    csv_reader   = read_csv(file.toStdString().c_str());
    if (csv_reader != NULL)
    {
        num_mea = csv_reader->numsteps;
        vars.clear();
        first.clear();
        last.clear();
        for(int i=0;i<csv_reader->numvars;i++)
        {
            variable v;

            v.name    = csv_reader->variables[i];
            v.type    = FMI_UNDEFINED;
            v.desc    = sEmpty;
            v.isParam = false;
            v.value   = sEmpty;
            v.index   = i;
            vars.append(v);

            double *vals = read_csv_dataset(csv_reader, csv_reader->variables[i]);
            first.append(vals!=NULL ? vals[0] : 0);
            last.append(vals!=NULL ? vals[csv_reader->numsteps-1] : 0);

        }
        omc_free_csv_reader(csv_reader);
        return FILE_OK;
    }
    return FILE_WRONG_FORMAT;
}

unsigned readVarsMATfile(QString file, QList<variable> &vars, QList<double> &first, QList<double> &last,  int &num_mea, QString &error)
{
    ModelicaMatReader mat;

    error = omc_new_matlab4_reader(file.toStdString().c_str(),&mat);
    if (error == 0)
    {
        num_mea = mat.nrows;
        vars.clear();
        first.clear();
        last.clear();
        for(unsigned int i=0;i<mat.nall;i++)
        {
            variable v;

            v.name = mat.allInfo[i].name;
            v.desc = mat.allInfo[i].descr;
            v.type = FMI_UNDEFINED;
            v.isParam = mat.allInfo[i].isParam == 1;
            v.index = mat.allInfo[i].index;
            v.value = v.isParam ? QString::number(mat.params[v.index],NUM_FORMAT,NUM_DEC) : sEmpty;
            vars.append(v);

            if (!v.isParam)
            {
                double *data = omc_matlab4_read_vals(&mat,v.index);
                first.append(data[0]);
                last.append(data[mat.nrows-1]);
            }
            else
            {
                first.append(0);
                last.append(0);
            }
        }
        omc_free_matlab4_reader(&mat);
        return FILE_OK;
    }
    return FILE_WRONG_FORMAT;
}

unsigned checkInputFile(QString file, unsigned &format, QList<variable> &vars, QList<double> &first, QList<double> &last, int &num_mea, QString &error)
{
    QString ext;

    // Check file exist and supported format
    unsigned res = checkFileExistAndSupported(file,ext);
    if (res!=FILE_OK) return res;

    // Check file format
    if (ext == eCSV)
    {
        format = ffCSV;
        return readVarsCSVfile(file,vars,first,last,num_mea,error);
    }
    else if (ext == eMAT || ext == eTXT)
    {
        format = ffTRJ;
        return readVarsMATfile(file,vars,first,last,num_mea,error);
    }
    else return FILE_UNSUPPORTED;
}

QString formatedUnit(opt_model m, QList<variable> v, int k)
{
    QString quantity = v[k].typeDef >= 0 ? m.types[v[k].typeDef].quantity : sEmpty;
    QString unit     = v[k].typeDef >= 0 ? m.types[v[k].typeDef].unit     : sEmpty;
    QString unitStr  = quantity.isEmpty() ? (unit.isEmpty() ? sEmpty   : quantity) :
                                            (unit.isEmpty() ? quantity : quantity + space + unitLeft + unit + unitRight);

    return unitStr;
}

void populateTreeGeneric(QTreeWidget *tree, QList<variable> vars, opt_project prj, int COL_NAME, QString var_selected, setItemTreePointer sit, QList<QWidget *> &lw, QTreeWidgetItem *topItem)
{
    // Populate tree
    QTreeWidgetItem *parentItem = NULL;
    QList<QTreeWidgetItem *> list;

    tree->setUpdatesEnabled(false);
    tree->setSortingEnabled(false);
    for (int k=0;k<vars.size();k++)
    {
        QString varname = vars[k].name;

        // do not split for der(x.a.b) - change to der(x&a&b)
        int id1 = varname.indexOf("der(");
        int id2 = varname.indexOf(")");
        if (id1>=0 && id2>=0 && id1<id2)
        {
            for(int i=id1;i<id2;i++)
                if (varname[i] == '.')
                    varname[i] = '&';
        }

        QStringList splitName = varname.split(".");

        // Restore der(x&a&b) to der(x.a.b)
        for(int i=0;i<splitName.count();i++)
        {
            int id1 = splitName[i].indexOf("der(");
            int id2 = splitName[i].indexOf(")");
            if (id1>=0 && id2>=0 && id1<id2)
            {
                for(int j=id1;j<id2;j++)
                    if (splitName[i][j] == '&')
                        splitName[i][j] = '.';
            }
        }


        // add root as top level item if treeWidget doesn't already have it
        if (topItem==NULL)
            list = tree->findItems(splitName[0], Qt::MatchCaseSensitive);
        else
        {
            bool found = false;
            int  i     = 0;

            list.clear();
            while (!found && i<topItem->childCount())
            {
                found = splitName[0] == topItem->child(i)->text(COL_NAME);
                if (found) list.append(topItem->child(i));
                i++;
            }
        }

        if (list.isEmpty())
        {
            parentItem = topItem==NULL ? new QTreeWidgetItem : new QTreeWidgetItem(topItem);
            parentItem->setText(COL_NAME, splitName[0]);
            parentItem->setIcon(COL_NAME, QIcon(iStruct));
            if (topItem==NULL) tree->addTopLevelItem(parentItem);
        }
        else
            parentItem = list[0];

        // iterate through non-root items
        for (int i = 1; i < splitName.size() - 1; ++i)
        {
            // iterate through children of parentItem to see if this item exists
            bool thisItemExists = false;
            for (int j = 0; j < parentItem->childCount(); ++j)
            {
                if (splitName[i] == parentItem->child(j)->text(COL_NAME))
                {
                    thisItemExists = true;
                    parentItem = parentItem->child(j);
                    break;
                }
            }

            if (!thisItemExists)
            {
                parentItem = new QTreeWidgetItem(parentItem);
                parentItem->setText(COL_NAME, splitName[i]);
                parentItem->setIcon(COL_NAME, QIcon(iStruct));
            }
        }

        if (splitName.size()>1)
        {
            QTreeWidgetItem *childItem = new QTreeWidgetItem(parentItem);

            childItem->setText(COL_NAME, splitName.last());
            sit(tree,childItem,prj,vars[k],var_selected,k,lw);
        }
        else
            sit(tree,parentItem,prj,vars[k],var_selected,k,lw);
    }
    tree->setUpdatesEnabled(true);
}


//void populateTreeGeneric(QTreeWidget *tree, QList<variable> vars, opt_model m, opt_exp e, int COL_NAME, QString var_selected, setItemTreePointer sit, QTreeWidgetItem *topItem)
//{
//    // Populate tree
//    QTreeWidgetItem *parentItem = NULL;
//    QList<QTreeWidgetItem *> list;

//    tree->setUpdatesEnabled(false);
//    tree->setSortingEnabled(false);
//    for (int k=0;k<vars.size();k++)
//    {
//        QString varname = vars[k].name;

//        // do not split for der(x.a.b) - change to der(x&a&b)
//        int id1 = varname.indexOf("der(");
//        int id2 = varname.indexOf(")");
//        if (id1>=0 && id2>=0 && id1<id2)
//        {
//            for(int i=id1;i<id2;i++)
//                if (varname[i] == '.')
//                    varname[i] = '&';
//        }

//        QStringList splitName = varname.split(".");

//        // Restore der(x&a&b) to der(x.a.b)
//        for(int i=0;i<splitName.count();i++)
//        {
//            int id1 = splitName[i].indexOf("der(");
//            int id2 = splitName[i].indexOf(")");
//            if (id1>=0 && id2>=0 && id1<id2)
//            {
//                for(int j=id1;j<id2;j++)
//                    if (splitName[i][j] == '&')
//                        splitName[i][j] = '.';
//            }
//        }


//        // add root as top level item if treeWidget doesn't already have it
//        if (topItem==NULL)
//            list = tree->findItems(splitName[0], Qt::MatchCaseSensitive);
//        else
//        {
//            bool found = false;
//            int  i     = 0;

//            list.clear();
//            while (!found && i<topItem->childCount())
//            {
//                found = splitName[0] == topItem->child(i)->text(COL_NAME);
//                if (found) list.append(topItem->child(i));
//                i++;
//            }
//        }

//        if (list.isEmpty())
//        {
//            parentItem = topItem==NULL ? new QTreeWidgetItem : new QTreeWidgetItem(topItem);
//            parentItem->setText(COL_NAME, splitName[0]);
//            parentItem->setIcon(COL_NAME, QIcon(iStruct));
//            if (topItem==NULL) tree->addTopLevelItem(parentItem);
//        }
//        else
//            parentItem = list[0];

//        // iterate through non-root items
//        for (int i = 1; i < splitName.size() - 1; ++i)
//        {
//            // iterate through children of parentItem to see if this item exists
//            bool thisItemExists = false;
//            for (int j = 0; j < parentItem->childCount(); ++j)
//            {
//                if (splitName[i] == parentItem->child(j)->text(COL_NAME))
//                {
//                    thisItemExists = true;
//                    parentItem = parentItem->child(j);
//                    break;
//                }
//            }

//            if (!thisItemExists)
//            {
//                parentItem = new QTreeWidgetItem(parentItem);
//                parentItem->setText(COL_NAME, splitName[i]);
//                parentItem->setIcon(COL_NAME, QIcon(iStruct));
//            }
//        }

//        if (splitName.size()>1)
//        {
//            QTreeWidgetItem *childItem = new QTreeWidgetItem(parentItem);

//            childItem->setText(COL_NAME, splitName.last());
//            sit(tree,childItem,m,e,vars[k],var_selected,k);
//        }
//        else
//            sit(tree,parentItem,m,e,vars[k],var_selected,k);
//    }
//    tree->setUpdatesEnabled(true);
//}

void setBackgroundColor(QLineEdit *w, QColor c)
{
    w->setStyleSheet("QLineEdit { background-color: "+ c.name() + " }");
}

void setBackgroundColor(QComboBox *w, QColor c)
{
    w->setStyleSheet("QComboBox { background-color: "+ c.name() + "; " +
                                 "selection-background-color: " + c.name() + " }");
}

void setLineStyleCombo(QComboBox *cb, QPen pen)
{
    double lwidth  = cb->width()  - 30;
    double lheight = cb->height() - 10;
    bool   state;

    state = cb->blockSignals(true);
    cb->clear();
    cb->setIconSize(QSize(lwidth,lheight));
    setBackgroundColor(cb,Qt::white);
    for (int i = Qt::NoPen; i < Qt::CustomDashLine; i++)
    {
        QPixmap pix(lwidth,lheight);
        QPen   lpen;
        double pos = lheight/2;

        pix.fill(pen.color() != Qt::white ? Qt::white : Qt::black);
        lpen.setColor(pen.color());
        lpen.setWidth(pen.width());
        lpen.setStyle((Qt::PenStyle)i);

        QPainter painter(&pix);
        painter.setPen(lpen);
        painter.drawLine(0,pos,lwidth,pos);
        cb->addItem(QIcon(pix),sEmpty);
    }
    cb->setCurrentIndex(pen.style());
    cb->blockSignals(state);
}

void setLineStyleLabel(QLabel *l, QPen pen)
{
    QPixmap pix(l->width(),l->height());
    QPainter painter(&pix);

    pix.fill(Qt::white);
    painter.setPen(pen);
    painter.drawLine(0,l->height()/2,l->width(),l->height()/2);
    l->setPixmap(pix);
}

void setBrushStyleLabel(QLabel *l, QBrush brush)
{
    QPixmap pix(l->width(),l->height());
    QPainter painter(&pix);

    pix.fill(Qt::white);
    painter.setBrush(brush);
    painter.drawRect(0,0,l->width(),l->height());
    l->setPixmap(pix);
}

void setBrushStyleCombo(QComboBox *cb, QBrush brush)
{
    double lwidth  = cb->width()  - 30;
    double lheight = cb->height() - 10;
    bool   state;

    state = cb->blockSignals(true);
    cb->clear();
    cb->setIconSize(QSize(lwidth,lheight));
    setBackgroundColor(cb,Qt::white);
    for (int i = Qt::NoBrush; i < Qt::TexturePattern; i++)
    {
        QPixmap pix(lwidth,lheight);
        QBrush  lbrush;
        QRect   rect;

        pix.fill(Qt::white);
        lbrush.setColor(brush.color());
        lbrush.setStyle((Qt::BrushStyle)i);

        rect.setX(0);
        rect.setY(0);
        rect.setWidth(lwidth);
        rect.setHeight(lheight);

        QPainter painter(&pix);
        painter.setBrush(lbrush);
        painter.drawRect(rect);
        cb->addItem(QIcon(pix),sEmpty);
    }
    cb->setCurrentIndex(brush.style());
    cb->blockSignals(state);
}

QString fontToString(QFont f)
{
    return "[ " + f.family() + ", " + QString::number(f.pointSize()) +" ]";
}

int lineWidth(QString s, QFont f)
{
    f.setBold(true);
    QFontMetrics fm(f);

    return fm.width(s);
}

int lineHeight(QFont f)
{
    QFontMetrics fm(f);

    return fm.height();
}

variable varTimeDef()
{
    variable time;

    time.name    = "time";
    time.type    = FMI_REAL;
    time.isParam = false;
    time.desc    = "Simulation time";
    time.value   = sEmpty;
    time.min     = sEmpty;
    time.max     = sEmpty;
    time.typeDef = -1;
    time.index   = -1;
    time.cau     = FMU_INDEPENDENT;
    time.var     = FMU_CONTINUOUS;

    return time;
}

void openFileInEditor(QString file, QString editor)
{
    QProcess process;

    if (file.trimmed().isEmpty())
    {
        process.startDetached(editor);
    }
    else
    {
        QStringList arg;

        arg.push_back(file);
        process.startDetached(editor,arg);
    }
}

QObject *findParentWithMethod(QObject *obj, QString methodSig)
{
    QObject *parent = obj->parent();
    bool     found  = false;

    while(!found && parent!=NULL)
    {
        const QMetaObject* metaObject = parent->metaObject();
        int   i = metaObject->methodOffset();

        while(!found && i<metaObject->methodCount())
        {
            found = QString::fromLatin1(metaObject->method(i).methodSignature()) == methodSig;
            i++;
        }
        if (!found) parent = parent->parent();
    }
    return parent;
}

void setItemTreeParams(QTreeWidget *tree, QTreeWidgetItem *item, opt_project prj, variable v, QString vs, int k, QList<QWidget *> &lw)
{
    Q_UNUSED(v);
    Q_UNUSED(vs);

    opt_model m = prj.getModel();

    item->setData(VAR_POS,Qt::UserRole,k);
    item->setData(VAR_VALUE,Qt::UserRole,m.params[k].value);
    item->setText(PAR_COL_DESC,m.params[k].desc);
    item->setText(PAR_COL_UNIT,formatedUnit(m,m.params,k));
    item->setText(PAR_COL_MIN,m.params[k].min);
    item->setText(PAR_COL_MAX,m.params[k].max);
    switch (m.params[k].type)
    {
        case FMI_INTEGER:
        {
            QSpinBox *sb = new QSpinBox(tree);
            double     min  = m.params[k].min.isEmpty() ? std::numeric_limits<int>::lowest() : m.params[k].min.toInt();
            double     max  = m.params[k].max.isEmpty() ? std::numeric_limits<int>::max()    : m.params[k].max.toInt();

            lw.append(sb);
            sb->setProperty(PAR_PRO_POS.toLatin1().data(),k);
            sb->setProperty(PAR_PRO_VALUE.toLatin1().data(),m.params[k].value);
            sb->setValue(m.params[k].value.toInt());
            sb->setMinimum(min);
            sb->setMaximum(max);
            sb->connect(sb,SIGNAL(valueChanged(int)),
                        findParentWithMethod(tree,"on_sb_valueChanged(int)"),SLOT(on_sb_valueChanged(int)));
            item->setIcon(PAR_COL_NAME,QIcon(iInteger));
            tree->setItemWidget(item,PAR_COL_VALUE,sb);
            // Changed parameter value
            int ix = m.p_name_new.indexOf(m.params[k].name);
            if (ix>=0)
            {
                int val;
                bool ok;

                val = m.p_value_new[ix].toInt(&ok);
                if (ok) sb->setValue(val);
            }
            break;
        }
        case FMI_REAL:
        {
            QLineEdit        *ler  = new QLineEdit(tree);
            QDoubleValidator *dv   = new QDoubleValidator;
            QString           tTip = "Range: " + (m.params[k].min.isEmpty() ? "( -Inf , " : "[ " + m.params[k].min + ", ") +
                                                 (m.params[k].max.isEmpty() ? "Inf )" : m.params[k].max + " ]");
            double     min  = m.params[k].min.isEmpty() ? std::numeric_limits<double>::lowest() : m.params[k].min.toDouble();
            double     max  = m.params[k].max.isEmpty() ? std::numeric_limits<double>::max()    : m.params[k].max.toDouble();

            lw.append(ler);
            ler->setProperty(PAR_PRO_POS.toLatin1().data(),k);
            ler->setProperty(PAR_PRO_VALUE.toLatin1().data(),m.params[k].value);
            dv->setLocale(QLocale::English);
            dv->setBottom(min);
            dv->setTop(max);
            ler->setValidator(dv);
            ler->setText(m.params[k].value);            
            ler->connect(ler,SIGNAL(textChanged(const QString &)),
                         findParentWithMethod(tree,"on_le_textChanged(QString)"),SLOT(on_le_textChanged(const QString &)));
            ler->setToolTip(tTip);
            item->setIcon(PAR_COL_NAME,QIcon(iReal));
            tree->setItemWidget(item,PAR_COL_VALUE,ler);
            // Changed parameter value
            int ix = m.p_name_new.indexOf(m.params[k].name);
            if (ix>=0)
            {
                bool ok;

                m.p_value_new[ix].toDouble(&ok);
                if (ok) ler->setText(m.p_value_new[ix]);
            }
            //qDebug() << m.params[k].name << m.params[k].value << min << max;
            break;
        }
        case FMI_STRING:
        {
            QLineEdit *les = new QLineEdit(tree);
            lw.append(les);
            les->setProperty(PAR_PRO_POS.toLatin1().data(),k);
            les->setProperty(PAR_PRO_VALUE.toLatin1().data(),m.params[k].value);
            les->setText(m.params[k].value);
            les->connect(les,SIGNAL(textChanged(const QString &)),
                         findParentWithMethod(tree,"on_le_textChanged(QString)"),SLOT(on_le_textChanged(const QString &)));
            item->setIcon(PAR_COL_NAME,QIcon(iString));
            tree->setItemWidget(item,PAR_COL_VALUE,les);
            // Changed parameter value
            int ix = m.p_name_new.indexOf(m.params[k].name);
            if (ix>=0) les->setText(m.p_value_new[ix]);
            break;
        }
        case FMI_BOOL:
        {
            QComboBox *cb = new QComboBox(tree);
            lw.append(cb);
            cb->setProperty(PAR_PRO_POS.toLatin1().data(),k);
            cb->setProperty(PAR_PRO_VALUE.toLatin1().data(),m.params[k].value);
            cb->addItems(QStringList() << sTrue << sFalse);
            cb->setCurrentText(m.params[k].value);
            cb->connect(cb,SIGNAL(currentTextChanged(const QString &)),
                        findParentWithMethod(tree,"on_cb_textChanged(QString)"),SLOT(on_cb_textChanged(const QString &)));
            item->setIcon(PAR_COL_NAME,QIcon(iBool));
            tree->setItemWidget(item,PAR_COL_VALUE,cb);
            // Changed parameter value
            int ix = m.p_name_new.indexOf(m.params[k].name);
            if (ix>=0 && (m.p_value_new[ix] == sTrue || m.p_value_new[ix] == sFalse)) cb->setCurrentText(m.p_value_new[ix]);
            break;
        }
        default:
            break;
    }
}

bool axisAlign(QAbstractSeries *s, Qt::AlignmentFlag af)
{
    for(int i=0;i<s->attachedAxes().count();i++)
    {
        if (s->attachedAxes()[i]->alignment() == af)
            return true;
    }
    return false;
}

void getMinMaxChart(QChartView *qcv, Qt::AlignmentFlag y_align, double &minT, double &maxT, double &minY, double &maxY)
{
    const double PER_BRD = 0.05;

    minT = std::numeric_limits<double>::max();
    maxT = std::numeric_limits<double>::lowest();
    minY = std::numeric_limits<double>::max();
    maxY = std::numeric_limits<double>::lowest();

    // Check min and max for all series in chart
    for(int i=0;i<qcv->chart()->series().count();i++)
    {
        QAbstractSeries *s = qcv->chart()->series()[i];

        if (s->property(MIN_T.toStdString().c_str()) < minT) minT = s->property(MIN_T.toStdString().c_str()).toDouble();
        if (s->property(MAX_T.toStdString().c_str()) > maxT) maxT = s->property(MAX_T.toStdString().c_str()).toDouble();
        if (axisAlign(s,y_align) || s->attachedAxes().count()==0)
        {
            if (s->property(MIN_Y.toStdString().c_str()) < minY) minY = s->property(MIN_Y.toStdString().c_str()).toDouble();
            if (s->property(MAX_Y.toStdString().c_str()) > maxY) maxY = s->property(MAX_Y.toStdString().c_str()).toDouble();
        }
    }

    // WARNING: for border in graphs
    minY = minY - abs(maxY-minY) * PER_BRD;
    maxY = maxY + abs(maxY-minY) * PER_BRD;

    // If min = max set a range in percentage
    rangeSameValues(minT,maxT,RANGE_T);
    rangeSameValues(minY,maxY,RANGE_Y);
}

void setAxesRangeLine(QChartView *qcv, Qt::AlignmentFlag y_align, bool isEmpty)
{
    const Qt::AlignmentFlag x_align = Qt::AlignBottom;
    QValueAxis    *aX = new QValueAxis(qcv);
    QValueAxis    *aY = new QValueAxis(qcv);
    int pos;

    double minT,maxT,minY,maxY;



    // Get min & max from chart
    if (isEmpty)
    {
        minT = 0;
        maxT = 1;
        minY = 0;
        maxY = 1;
    }
    else
        getMinMaxChart(qcv,y_align,minT,maxT,minY,maxY);

    // Current axes range
    aX->setRange(minT,maxT);
    aY->setRange(minY,maxY);

    // Remove series axes
    for(int i=0;i<qcv->chart()->series().count();i++)
    {
        QAbstractSeries *s = qcv->chart()->series()[i];

        pos = 0;
        while (pos<s->attachedAxes().count())
        {
            if (s->attachedAxes()[pos]->alignment() == y_align ||
                s->attachedAxes()[pos]->alignment() == x_align)
                s->detachAxis(s->attachedAxes()[pos]);
            else
                pos++;
        }
    }

    // Remove chart axes
    pos=0;
    while (pos<qcv->chart()->axes().count())
    {
        if (qcv->chart()->axes()[pos]->alignment() == y_align ||
            qcv->chart()->axes()[pos]->alignment() == x_align )
            qcv->chart()->removeAxis(qcv->chart()->axes()[pos]);
        else
            pos++;
    }

    // Set axes for chart
    if (qcv->chart()->series().count()>0)
    {
        qcv->chart()->addAxis(aX,x_align);
        qcv->chart()->addAxis(aY,y_align);
    }

    // Add axes to all series
    for(int i=0;i<qcv->chart()->series().count();i++)
    {
        QAbstractSeries *s = qcv->chart()->series()[i];

        if (axisAlign(s,y_align) || s->attachedAxes().count() == 0)
            s->attachAxis(aY);
        s->attachAxis(aX);
    }
}

void rangeSameValues(double &min, double &max, double RANGE)
{
    // Practically same values
    if ((max-min)<MIN_RANGE)
    {
        double aux = qMax(min,max);

        min = aux - RANGE;
        max = aux + RANGE;
    }
}

bool focusItemGraph(QWidget *&w)
{
    w = QApplication::focusWidget();
    return (w != NULL && w->inherits(cQChartView.toStdString().c_str()));
}

void setEmptyAxes(QtCharts::QChartView *qcv)
{
    QValueAxis *aX = new QValueAxis;
    QValueAxis *aY = new QValueAxis;

    aX->setRange(DEF_AXIS_X_MIN,DEF_AXIS_X_MAX);
    aY->setRange(DEF_AXIS_Y_MIN,DEF_AXIS_Y_MAX);
    qcv->chart()->addAxis(aX,Qt::AlignBottom);
    qcv->chart()->addAxis(aY,Qt::AlignLeft);
}

QString removeHTML(QString htmlString)
{
    QXmlStreamReader xml(htmlString);
    QString textString;

    while (!xml.atEnd())
    {
        if (xml.readNext() == QXmlStreamReader::Characters)
            textString += xml.text();
    }
    return textString;
}

void graphHintConfig(QTextEdit *&txt, QStringList msg, QColor c)
{
    int      widht   = -1;
    int      height  = lineHeight(txt->font()) + 2;
    QString  msgLine = sEmpty;
    QString  bColor  = c.red() >= 240 && c.green() >= 240 ? "#A0DAEE" : "#FFFF7F";


    txt->setStyleSheet(QString(cHintStyle).arg(bColor));
    txt->setHorizontalScrollBarPolicy(Qt::ScrollBarPolicy::ScrollBarAlwaysOff);
    txt->setVerticalScrollBarPolicy(Qt::ScrollBarPolicy::ScrollBarAlwaysOff);
    txt->setReadOnly(true);
    txt->setFrameShape(QFrame::NoFrame);
    txt->setLineWrapMode(QTextEdit::NoWrap);
    txt->setWordWrapMode(QTextOption::NoWrap);
    for(int i=0;i<msg.count();i++)
    {
        int lwidth = lineWidth(removeHTML(msg[i]),txt->font());

        widht    = lwidth  > widht  ? lwidth  : widht;
        msgLine += msg[i];
        if (i<msg.count()-1) msgLine += '\n';
    }
    txt->setFixedWidth(widht+15);
    txt->setFixedHeight(height*msg.count());
    txt->setText(msgLine);
}

double distance(QPointF a, QPointF b)
{
    return sqrt(pow(a.x()-b.x(),2) + pow(a.y()-b.y(),2));
}

QPointF findPoint(QList<QPointF> points, QPointF p)
{
    for(int i=0;i<points.count();i++)
    {
        if (points[i].x() > p.x())
        {
            if (i == 0) return points[i];
            if (distance(points[i],p)<distance(points[i-1],p))
                return points[i];
            else
                return points[i-1];
        }
    }
    return points.count()>0 ?
           distance(points.last(),p) < distance(points.first(),p) ?
           points.last() : points.first() : QPointF();
}

bool selectedSerie()
{
    return serie;
}

void sliceSerieHoverInfo(QChart *chart, QPieSlice *slice, QString name, QPointF point, QPointF &pChart, QStringList &msg)
{
    const QString lineName = "<span style=\"font-weight:600;\"><span style=\"color:%1;\">%2</span><br/>";
    const QString lineX    = "<span style=\"font-weight:600;\"><span style=\"color:%1;\">Time: </span>%2</span><br/>";
    const QString lineY    = "<span style=\"font-weight:600;\"><span style=\"color:%1;\">Perc: </span>%2</span>\%<br/>";

    // Map point to position
    QPointF p1 = chart->mapToScene(point);
    pChart     = chart->mapToParent(point);

    qDebug() << "Mouse: " << point.x() << point.y();
    qDebug() << "P1: " << p1.x() << p1.y();
    qDebug() << "P2: " << pChart.x() << pChart.y();

    // Messages
    msg.append(QString(lineName).arg(slice->color().name(),name));
    msg.append(QString(lineX).arg( slice->color().name(),QString::number(0 ,NUM_FORMAT,NUM_DEC)));
    msg.append(QString(lineY).arg( slice->color().name(),QString::number(slice->value(),NUM_FORMAT,NUM_DEC)));
}

void LineSerieHoverInfo(QChart *chart, QLineSeries *series, QString name, QPointF point, QPointF &pChart, QStringList &msg, QPen &pen, QBrush &brush, QColor &color)
{
    const QString lineName = "<span style=\"font-weight:600;\"><span style=\"color:%1;\">%2</span><br/>";
    const QString lineX    = "<span style=\"font-weight:600;\"><span style=\"color:%1;\">X: </span>%2</span><br/>";
    const QString lineY    = "<span style=\"font-weight:600;\"><span style=\"color:%1;\">Y: </span>%2</span><br/>";

    // Find an exact point in series
    QPointF pSerie = findPoint(series->points(),point);

    // Map point to position
    pChart = chart->mapToPosition(pSerie,series);

    // Pen, brush & color
    pen   = series->pen();
    color = series->color();
    brush.setStyle(Qt::SolidPattern);
    brush.setColor(pen.color());
    pen.setColor(Qt::black);

    // Messages
    msg.append(QString(lineName).arg(series->color().name(),name));
    msg.append(QString(lineX).arg(series->color().name(),QString::number(pSerie.x(),NUM_FORMAT,NUM_DEC)));
    msg.append(QString(lineY).arg(series->color().name(),QString::number(pSerie.y(),NUM_FORMAT,NUM_DEC)));
}

void AreaSerieHoverInfo(QChart *chart, QAreaSeries *series, QString name, QPointF point, QPointF &pChart, QStringList &msg, QPen &pen, QBrush &brush, QColor &color)
{
    const QString lineName = "<span style=\"font-weight:600;\"><span style=\"color:%1;\">%2</span><br/>";
    const QString lineX    = "<span style=\"font-weight:600;\"><span style=\"color:%1;\">X: </span>%2</span><br/>";
    const QString lineY    = "<span style=\"font-weight:600;\"><span style=\"color:%1;\">Y: </span>%2</span><br/>";
    const QString lineY1   = "<span style=\"font-weight:600;\"><span style=\"color:%1;\">Y1: </span>%2</span><br/>";
    const QString lineY2   = "<span style=\"font-weight:600;\"><span style=\"color:%1;\">Y2: </span>%2</span><br/>";
    const QString lineInt  = "<span style=\"font-weight:600;\"><span style=\"color:%1;\">&#x394;Y: </span>%2</span><br/>";

    // Map point to position
    pChart = chart->mapToPosition(point,series);

    // Points
    QPointF pLower = findPoint(series->lowerSeries()->points(),point);
    QPointF pUpper = findPoint(series->upperSeries()->points(),point);

    // Pen, brush & color
    pen   = series->pen();
    color = series->color();
    brush.setStyle(Qt::SolidPattern);
    brush.setColor(pen.color());
    pen.setColor(Qt::black);

    // Messages
    msg.append(QString(lineName).arg(series->color().name(),name));
    msg.append(QString(lineX).arg(  series->color().name(),QString::number(point.x() ,NUM_FORMAT,NUM_DEC)));
    msg.append(QString(lineY).arg(  series->color().name(),QString::number(point.y() ,NUM_FORMAT,NUM_DEC)));
    msg.append(QString(lineY1).arg( series->color().name(),QString::number(pLower.y(),NUM_FORMAT,NUM_DEC)));
    msg.append(QString(lineY2).arg( series->color().name(),QString::number(pUpper.y(),NUM_FORMAT,NUM_DEC)));
    msg.append(QString(lineInt).arg(series->color().name(),QString::number(pUpper.y()-pLower.y(),NUM_FORMAT,NUM_DEC)));
}

void BarSerieHoverInfo(QBarSet *series, int index, QPointF point, QPointF &pChart, QStringList &msg, QPen &pen, QBrush &brush, QColor &color)
{
    const QString lineName  = "<span style=\"font-weight:600;\"><span style=\"color:%1;\">%2</span><br/>";
    const QString lineValue = "<span style=\"font-weight:600;\"><span style=\"color:%1;\">Y: </span>%2</span><br/>";

    // Map point from cursor to chart position
    if (series!=NULL)
    {
        // Pointer to ChartView
        QChartView *view = (QChartView *)series->property(VAR_CHARTVIEW.toStdString().c_str()).value<void *>();

        if (view!=NULL)
        {                   
            pChart = view->mapFromGlobal(QPoint(point.x(),point.y()));

            // Pen, brush & color
            pen.setColor(series->brush().color());
            color = series->color();
            brush.setStyle(Qt::SolidPattern);
            brush.setColor(pen.color());
            pen.setColor(Qt::black);

            // Messages
            msg.append(QString(lineName).arg(series->color().name(),series->label()));
            msg.append(QString(lineValue).arg(series->color().name(),QString::number(series->at(index),NUM_FORMAT,NUM_DEC)));
        }
    }
}

void sliceHover(bool state, QObject *sender)
{
    if (sender!=NULL)
    {
        QPieSlice  *slice = qobject_cast<QPieSlice *>(sender);
        //QPieSeries *serie = slice!=NULL ? slice->series() : NULL;
        //QChart     *chart = serie!=NULL ? qobject_cast<QChart *>(serie->chart()) : NULL;

        if (slice!=NULL)
        {
            slice->setExploded(state);
            slice->setLabelVisible(state);

//            if (state)
//            {
//                mutex.lock();

//                // Info inside the hint box
//                const int      nMaxLen = 12;
//                const QString  nMaxCad = "...";
//                QString        sName = slice->label().count()>nMaxLen ? slice->label().mid(0,nMaxLen-nMaxCad.length()) + nMaxCad : slice->label();
//                QStringList    msg;
//                QPointF        pChart;

//                sliceSerieHoverInfo(chart,slice,sName,QCursor::pos(),pChart,msg);

//                if (chart!=NULL && chart->scene()!=NULL)
//                {
//                    QGraphicsProxyWidget *qpw = NULL;

//                    if (txt == NULL) txt = new QTextEdit;
//                    if (txt != NULL) qpw = chart->scene()->addWidget(txt);

//                    // Info text
//                    if (txt!=NULL) graphHintConfig(txt,msg,slice->color());
//                    if (qpw!=NULL)
//                    {
//                        // Position
//                        qpw->setX(pChart.x() + 15);
//                        qpw->setY(pChart.y() + 5);

//                        // Hint box size
//                        int w = txt->width();
//                        int h = txt->height();

//                        // Right chart margin
//                        if (chart->size().width() - qpw->x() < w)
//                            qpw->setX(pChart.x() - w - 5);

//                        // Bottom chart margin
//                        if (chart->size().height() - qpw->y() < h)
//                            qpw->setY(pChart.y() - h - 5);
//                    }
//                    mutex.unlock();
//                }
//                else
//                {
//                    mutex.lock();
//                    if (txt!=NULL) chart->scene()->removeItem(txt->graphicsProxyWidget());
//                    txt = NULL;
//                    mutex.unlock();
//                }
//            }
        }
    }
}

void removeSerieHint()
{
    mutex.lock();
    if (qge!=NULL)
    {
        delete qge;
        qge = NULL;
    }
    if (txt!=NULL)
    {
        txt->deleteLater();
        txt = NULL;
    }
    serie = false;
    mutex.unlock();
}

void serieHover(QPointF point, bool state, QObject *sender, QMenu *cmSerie, int index)
{
    if (sender!=NULL)
    {
        QAbstractSeries *series = qobject_cast<QAbstractSeries *>(sender);
        QLineSeries     *sl     = qobject_cast<QLineSeries *>(sender);
        QAreaSeries     *sa     = qobject_cast<QAreaSeries *>(sender);
        QBarSet         *sb     = qobject_cast<QBarSet *>(sender);
        QChart          *chart  = sl!=NULL || sa!=NULL ? series->chart() : NULL;

        if (sb!=NULL)
        {
            series = qobject_cast<QBarSeries *>(sb->parent());
            if (series!=NULL) chart = series->chart();
        }

        if (chart!=NULL)
        {
            if (state)
            {
                // Info to be defined per series
                QStringList msg;
                QPointF     pChart;
                QColor      color;
                QPen        pen;
                QBrush      brush;

                mutex.lock();

                // Reference to series
                if (cmSerie!=NULL) cmSerie->setProperty(REF_SER.toStdString().c_str(),qVariantFromValue((void *)series));

                // Info inside the hint box
                const int      nMaxLen = 12;
                const QString  nMaxCad = "...";
                QString        sName = series->name().count()>nMaxLen ? series->name().mid(0,nMaxLen-nMaxCad.length()) + nMaxCad : series->name();

                if (sl!=NULL) LineSerieHoverInfo(chart,sl,sName,point,pChart,msg,pen,brush,color);
                if (sa!=NULL) AreaSerieHoverInfo(chart,sa,sName,point,pChart,msg,pen,brush,color);
                if (sb!=NULL) BarSerieHoverInfo(sb,index,point,pChart,msg,pen,brush,color);

                if (chart->scene()!=NULL)
                {
                    QGraphicsProxyWidget *qpw = NULL;

                    if (txt == NULL) txt = new QTextEdit;
                    if (txt != NULL) qpw = chart->scene()->addWidget(txt);
                    if (qge != NULL) delete qge;
                    qge = chart->scene()->addEllipse(pChart.x()-POINT_SIZE/2,pChart.y()-POINT_SIZE/2,POINT_SIZE,POINT_SIZE,pen,brush);

                    // Info text
                    if (txt!=NULL) graphHintConfig(txt,msg,color);
                    if (qpw!=NULL)
                    {
                        // Position
                        qpw->setX(pChart.x() + 15);
                        qpw->setY(pChart.y() + 5);

                        // Hint box size
                        int w = txt->width();
                        int h = txt->height();

                        // Right chart margin
                        if (chart->size().width() - qpw->x() < w)
                            qpw->setX(pChart.x() - w - 5);

                        // Bottom chart margin
                        if (chart->size().height() - qpw->y() < h)
                            qpw->setY(pChart.y() - h - 5);
                    }
                    serie = sl!=NULL ? true : false;
                    mutex.unlock();
                }
            }
            else
            {
                removeSerieHint();
            }
        }
    }
}

void QChartViewExt::setObjNull()
{
    obj = NULL;
}

void QChartViewExt::mouseReleaseEvent(QMouseEvent *event)
{
    if(event->button() == Qt::RightButton) return; // Disable default right click in QChartView
    QChartView::mouseReleaseEvent(event);          // Any other event
}

void lineCoefficients(double x1, double y1, double x2, double y2, double &a, double &b)
{
    a  = (y2-y1)/(x2-x1);
    b  = y1 - a*x1;
}

bool pointWithRespectLine(double x1,double y1,double x2,double y2,double x,double y, bool below)
{
    double a,b,yc;

    lineCoefficients(x1,y1,x2,y2,a,b);
    yc = a*x+b;

    return below ? y<=yc : y>=yc;
}

bool pointInLine(double x1,double y1,double x2,double y2,double x,double y)
{
    double a,b,yc;

    lineCoefficients(x1,y1,x2,y2,a,b);
    yc = a*x+b;

    return y==yc;
}

void QChartViewExt::mouseMoveEvent(QMouseEvent *event)
{
    if (event->type() == QEvent::MouseMove)
    {
        // Value in chart
        QPointF pValue = chart()->mapToValue(event->pos());

        // Delete previous selected point in chart
        if(obj!=NULL)
        {
            serieHover(pValue,false,obj,NULL);
            obj = NULL;
        }

        // Locate that value inside area series
        for(int i=0;i<chart()->series().count();i++)
        {
            QLineSeries *sl = qobject_cast<QLineSeries *>(chart()->series()[i]);
            QAreaSeries *sa = qobject_cast<QAreaSeries *>(chart()->series()[i]);

            if(sa!=NULL)
            {
                int            j     = 0;
                bool           found = false;
                QList<QPointF> lsp   = sa->lowerSeries()->points();
                QList<QPointF> usp   = sa->upperSeries()->points();

                while(!found && j<lsp.count()-1)
                {

                    if (pValue.x()>=lsp[j].x() && pValue.x()<=lsp[j+1].x() &&
                        pointWithRespectLine(lsp[j].x(),lsp[j].y(),lsp[j+1].x(),lsp[j+1].y(),pValue.x(),pValue.y(),false) &&
                        pointWithRespectLine(usp[j].x(),usp[j].y(),usp[j+1].x(),usp[j+1].y(),pValue.x(),pValue.y(),true))
                    {
                        found = true;
                        obj   = sa;
                        serieHover(pValue,true,sa,NULL);
                    }
                    j++;
                }
            }
            else if(sl!=NULL)
            {
                QList<QPointF> lp = sl->points();
                bool found = false;
                int  j     = 0;

                while(!found && j<lp.count()-1)
                {
                    if(pointInLine(lp[j].x(),lp[j].y(),lp[j+1].x(),lp[j+1].y(),pValue.x(),pValue.y()))
                    {
                        found = true;
                        obj   = sl;
                        serieHover(pValue,true,sl,NULL);
                    }
                    j++;
                }
            }
        }

        QChartView::mouseMoveEvent(event);
    }
}

QString toLocalDecimalSeparator(QString s)
{
    const QLocale & cLocale = QLocale::system();

    s.replace(DECIMAL_SEP,cLocale.decimalPoint());
    return s;
}

QString copy(QTableWidget *table, int top, int left, int rowCount, int colCount, bool header, QString colSeparator, QString rowSeparator)
{
    QString str = sEmpty;

    //const QLocale & cLocale = QLocale::system();
    //QMessageBox msgBox;

    //msgBox.setText("Locale decimal separator: " + QString(cLocale.decimalPoint()));
    //msgBox.exec();


    if (header)
    {
        for(int j=left;j<left+colCount;j++)
        {
            QTableWidgetItem *item = table->horizontalHeaderItem(j);
            if (item!=NULL)
            {
                QString s = item->text();
                s.remove(QRegExp(HTML_EXP));
                int index = s.indexOf(sLessEqual);

                if (index>=0) s = s.mid(0,index);
                str += s;
                str += colSeparator;
            }
        }
        str = str.mid(0,str.count()-1) + rowSeparator;
    }

    for(int i=top;i<top+rowCount;i++)
    {
        for(int j=left;j<left+colCount;j++)
        {
            QTableWidgetItem *item = table->item(i,j);
            if (item!=NULL)
            {
                str += toLocalDecimalSeparator(item->text());
                str += colSeparator;
            }
        }
        str = str.mid(0,str.count()-1) + rowSeparator;
    }
    str = str.mid(0,str.count()-1);
    str.remove(QRegExp(HTML_EXP));
    return str;
}

void copyClipboard(QTableWidget *table, int top, int left, int rowCount, int colCount, bool header)
{
    QString str = copy(table,top,left,rowCount,colCount,header);
    QApplication::clipboard()->setText(str);
    qDebug() << str;
}


