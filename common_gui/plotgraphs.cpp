#include "plotgraphs.h"
#include "ui_plotgraphs.h"
#include "seriesconfig.h"
#include "graphconfig.h"
#include "common.h"

#include <QMessageBox>
#include <QLineSeries>
#include <QValueAxis>

const int COL_NAME    = 0;
const int COL_CAU     = 1;
const int COL_VAR     = 2;
const int COL_VALUE   = 3;
const int COL_DESC    = 4;
const int COL_UNIT    = 5;
const int COL_MIN     = 6;
const int COL_MAX     = 7;

const int WIDTH_NAME  = 150;
const int WIDTH_CAU   = 35;
const int WIDTH_VAR   = 35;
const int WIDTH_VALUE = 50;
const int WIDTH_DESC  = 100;
const int WIDTH_UNIT  = 100;
const int WIDTH_MIN   = 50;
const int WIDTH_MAX   = 50;

const int POS_DATA  = 0;
const int POS_VAR   = 1;
const int POS_SER   = 2;

const QString REF_ITEM = "REF_ITEM";
const QString REF_DATA = "REF_DATA";
const QString REF_VAR  = "REF_VAR";

const QStringList indNames     = {"time"};
const QString     cQSplitter   = "QSplitter";
const QString     cQLineSeries = "QtCharts::QLineSeries";

const QString PRO_WIDTH = "PRO_WIDTH";

const double DEF_AXIS_X_MIN = 0;
const double DEF_AXIS_X_MAX = 1;
const double DEF_AXIS_Y_MIN = 0;
const double DEF_AXIS_Y_MAX = 1;

const int HANDLE_WIDTH = 7;

void plotgraphs::setDefaultChart(QtCharts::QChartView *qcv)
{
    QBrush bWhite(Qt::white);

    qcv->setBackgroundBrush(bWhite);
    qcv->setFrameShape(QFrame::Shape::StyledPanel);
    qcv->chart()->legend()->hide();
    qcv->setRenderHint(QPainter::Antialiasing);
    setEmptyAxes(qcv);
    qcv->installEventFilter(this);
    qcv->chart()->setAcceptHoverEvents(true);
    qcv->setMouseTracking(true);
    qcv->setContextMenuPolicy(Qt::CustomContextMenu);
    qcv->setRubberBand(QChartView::RectangleRubberBand);
    connect(qcv, SIGNAL(customContextMenuRequested(const QPoint &)), this, SLOT(onCustomContextMenu(const QPoint &)));

    ui->menubar->setVisible(false);
    ui->toolBar->setVisible(false);
    ui->gbData->setVisible(false);
}

plotgraphs::plotgraphs(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::plotgraphs)
{
    ui->setupUi(this);

    setWindowTitle("Plotting tool");

    QStringList headerLabels;

    headerLabels.push_back(tName);
    headerLabels.push_back(tCau);
    headerLabels.push_back(tVar);
    headerLabels.push_back(tValue);
    headerLabels.push_back(tDesc);
    headerLabels.push_back(tQuantityUnit);
    headerLabels.push_back(tMin);
    headerLabels.push_back(tMax);
    ui->tree->setColumnCount(headerLabels.count());
    ui->tree->setHeaderLabels(headerLabels);

    ui->tree->setColumnWidth(COL_NAME,WIDTH_NAME);
    ui->tree->setColumnWidth(COL_CAU,WIDTH_CAU);
    ui->tree->setColumnWidth(COL_VAR,WIDTH_VAR);
    ui->tree->setColumnWidth(COL_VALUE,WIDTH_VALUE);
    ui->tree->setColumnWidth(COL_DESC,WIDTH_DESC);
    ui->tree->setColumnWidth(COL_UNIT,WIDTH_UNIT);
    ui->tree->setColumnWidth(COL_MIN,WIDTH_MIN);
    ui->tree->setColumnWidth(COL_MAX,WIDTH_MAX);

    ui->actionClear->setText("Clear graph");
    ui->actionConfigureGraph->setText("Configure graph");
    ui->actionConfigureSerie->setText("Configure series");
    ui->actionHorizontal->setText("Add graph to the right");
    ui->actionVertical->setText("Add graph below");
    ui->actionLoad_file->setText("Load file");
    ui->actionRemoveGraph->setText("Remove current graph");
    ui->actionRemoveSerie->setText("Remove series");
    ui->actionZoomOut->setText("Zoom out");
    ui->actionZoomReset->setText("Zoom reset");

    ui->actionClear->setIcon(QIcon(iWindow));
    ui->actionConfigureGraph->setIcon(QIcon(":icons/office-chart-area-black.svg"));
    ui->actionConfigureSerie->setIcon(QIcon(":icons/office-chart-area-black.svg"));
    ui->actionHorizontal->setIcon(QIcon(iHorizontal));
    ui->actionVertical->setIcon(QIcon(iVertical));
    ui->actionLoad_file->setIcon(QIcon(iLoadProject));
    ui->actionRemoveGraph->setIcon(QIcon(iListRemove));
    ui->actionRemoveSerie->setIcon(QIcon(iWindowClose));
    ui->actionZoomOut->setIcon(QIcon(iZoomOut));
    ui->actionZoomReset->setIcon(QIcon(iZoom));

    ui->tbVertical->setToolTip(ui->actionVertical->text());
    ui->tbHoriozontal->setToolTip(ui->actionHorizontal->text());
    ui->tbDeleteGraph->setToolTip(ui->actionRemoveGraph->text());
    ui->tbAddTab->setToolTip("Add a new tab");
    ui->tbDeleteTab->setToolTip("Delete the current tab");
    ui->tbClearGraph->setToolTip(ui->actionClear->text());
    ui->tbZoomOut->setToolTip(ui->actionZoomOut->text());
    ui->tbZoomReset->setToolTip(ui->actionZoomReset->text());

    ui->tbHoriozontal->setIcon(QIcon(iHorizontal));
    ui->tbVertical->setIcon(QIcon(iVertical));
    ui->tbDeleteGraph->setIcon(QIcon(iListRemove));
    ui->tbAddTab->setIcon(QIcon(iTabNew));
    ui->tbDeleteTab->setIcon(QIcon(iWindowClose));
    ui->tbClearGraph->setIcon(QIcon(iWindow));
    ui->tbZoomReset->setIcon(QIcon(iZoom));
    ui->tbZoomOut->setIcon(QIcon(iZoomOut));

    ui->tbHoriozontal->setFocusPolicy(Qt::NoFocus);
    ui->tbVertical->setFocusPolicy(Qt::NoFocus);
    ui->tbDeleteGraph->setFocusPolicy(Qt::NoFocus);
    ui->tbAddTab->setFocusPolicy(Qt::NoFocus);
    ui->tbDeleteTab->setFocusPolicy(Qt::NoFocus);
    ui->tbClearGraph->setFocusPolicy(Qt::NoFocus);
    ui->tbZoomReset->setFocusPolicy(Qt::NoFocus);
    ui->tbZoomOut->setFocusPolicy(Qt::NoFocus);

    ui->actionLight->setIcon(QIcon(iStyle));
    ui->actionBlue_cerulean->setIcon(QIcon(iStyle));
    ui->actionDark->setIcon(QIcon(iStyle));
    ui->actionBorwn_sand->setIcon(QIcon(iStyle));
    ui->actionBlue_NCS->setIcon(QIcon(iStyle));
    ui->actionHigh_contrast->setIcon(QIcon(iStyle));
    ui->actionBlue_icy->setIcon(QIcon(iStyle));
    ui->actionQt->setIcon(QIcon(iStyle));

    ui->actionLight->setText("Light");
    ui->actionBlue_cerulean->setText("Blue cerulean");
    ui->actionDark->setText("Dark");
    ui->actionBorwn_sand->setText("Brown sand");
    ui->actionBlue_NCS->setText("Blue NCS");
    ui->actionHigh_contrast->setText("High constrast");
    ui->actionBlue_icy->setText("Blue icy");
    ui->actionQt->setText("Qt");

    mStyle = new QMenu(tGraphStyle);
    mStyle->setIcon(QIcon(iStyle));
    mStyle->addSection("Style overrides current configuration");
    mStyle->addAction(ui->actionLight);
    mStyle->addAction(ui->actionBlue_cerulean);
    mStyle->addAction(ui->actionDark);
    mStyle->addAction(ui->actionBorwn_sand);
    mStyle->addAction(ui->actionBlue_NCS);
    mStyle->addAction(ui->actionHigh_contrast);
    mStyle->addAction(ui->actionBlue_icy);
    mStyle->addAction(ui->actionQt);

    cmSerie.addSection(tLineSeries);
    cmSerie.addAction(ui->actionConfigureSerie);
    cmSerie.addSeparator();
    cmSerie.addAction(ui->actionSerieClipboardText);
    cmSerie.addAction(ui->actionSerieFileText);
    cmSerie.addSeparator();
    cmSerie.addAction(ui->actionRemoveSerie);

    cmClipboardGraph.setIcon(QIcon(iClipboard));
    cmClipboardGraph.setTitle("Clipboard");
    cmClipboardGraph.addAction(ui->actionChartClipboardImage);
    cmClipboardGraph.addAction(ui->actionChartClipboardText);

    cmSaveGraph.setIcon(QIcon(iSave));
    cmSaveGraph.setTitle("Save");
    cmSaveGraph.addAction(ui->actionChartFileImage);
    cmSaveGraph.addAction(ui->actionChartFileText);

    cmGraph.addSection(tGraphs);
    cmGraph.addAction(ui->actionConfigureGraph);
    cmGraph.addMenu(mStyle);
    cmGraph.addSeparator();
    cmGraph.addAction(ui->actionHorizontal);
    cmGraph.addAction(ui->actionVertical);
    cmGraph.addSeparator();
    cmGraph.addAction(ui->actionZoomReset);
    cmGraph.addAction(ui->actionZoomOut);
    cmGraph.addSeparator();
    cmGraph.addMenu(&cmClipboardGraph);
    cmGraph.addMenu(&cmSaveGraph);
    cmGraph.addSeparator();
    cmGraph.addAction(ui->actionClear);
    cmGraph.addAction(ui->actionRemoveGraph);

    cmData.addSection(tData);
    cmData.addAction(ui->actionDataFileText);
    cmData.addSeparator();
    cmData.addAction(ui->actionRemoveData);

    while(ui->tabWidget->count()>0)
        ui->tabWidget->removeTab(0);
    tabCounter = 1;
    addDefaultTab();
}

plotgraphs::~plotgraphs()
{
    delete mStyle;
    delete ui;
}

void plotgraphs::clearAll()
{
    // Delete tab
    while (ui->tabWidget->count()>1)
        ui->tabWidget->removeTab(ui->tabWidget->currentIndex());

    // Delete graphs
    do{
        ui->actionRemoveGraph->trigger();
    }while(removedGraph);

    // Delete series in current graph
     ui->actionClear->trigger();

    // Delete data
    data.clear();

    // Delete tree
    ui->tree->clear();
}

void setItemTreePlot(QTreeWidget *tree, QTreeWidgetItem *item, opt_project prj, variable v, QString vs, int k, QList<QWidget *> &lw)
{
    Q_UNUSED(tree);
    Q_UNUSED(prj);
    Q_UNUSED(lw);

    if (item->childCount() == 0)
    {
        // Data type
        if      (v.type == FMI_REAL)    {item->setIcon(COL_NAME,QIcon(iReal));       item->setToolTip(COL_NAME,tReal); }
        else if (v.type == FMI_INTEGER) {item->setIcon(COL_NAME,QIcon(iInteger));    item->setToolTip(COL_NAME,tInteger); }
        else if (v.type == FMI_BOOL)    {item->setIcon(COL_NAME,QIcon(iBool));       item->setToolTip(COL_NAME,tBool); }
        else if (v.type == FMI_STRING)  {item->setIcon(COL_NAME,QIcon(iString));     item->setToolTip(COL_NAME,tString); }
        else                            {item->setIcon(COL_NAME,QIcon(iUndefined));  item->setToolTip(COL_NAME,tUndefined); }

        // Causality
        if      (v.cau == FMU_PARAMETER)   {item->setIcon(COL_CAU,QIcon(iParameter));   item->setToolTip(COL_CAU,tParam); }
        else if (v.cau == FMU_CALCULATED)  {item->setIcon(COL_CAU,QIcon(iCalculated));  item->setToolTip(COL_CAU,tCalculated); }
        else if (v.cau == FMU_INPUT)       {item->setIcon(COL_CAU,QIcon(iInput));       item->setToolTip(COL_CAU,tInput); }
        else if (v.cau == FMU_OUTPUT)      {item->setIcon(COL_CAU,QIcon(iOutput));      item->setToolTip(COL_CAU,tOutput); }
        else if (v.cau == FMU_LOCAL)       {item->setIcon(COL_CAU,QIcon(iLocal));       item->setToolTip(COL_CAU,tLocal); }
        else if (v.cau == FMU_INDEPENDENT) {item->setIcon(COL_CAU,QIcon(iIndependent)); item->setToolTip(COL_CAU,tIndependent); }

        // Variabity
        if      (v.var == FMU_CONSTANT)   {item->setIcon(COL_VAR,QIcon(iConstant));   item->setToolTip(COL_VAR,tConstant); }
        else if (v.var == FMU_FIXED)      {item->setIcon(COL_VAR,QIcon(iFixed));      item->setToolTip(COL_VAR,tFixed); }
        else if (v.var == FMU_TUNABLE)    {item->setIcon(COL_VAR,QIcon(iTunable));    item->setToolTip(COL_VAR,tTunable); }
        else if (v.var == FMU_DISCRETE)   {item->setIcon(COL_VAR,QIcon(iDiscrete));   item->setToolTip(COL_VAR,tDiscrete); }
        else if (v.var == FMU_CONTINUOUS) {item->setIcon(COL_VAR,QIcon(iContinuous)); item->setToolTip(COL_VAR,tContinuous); }

        // Description, unit, value, min and max
        item->setText(COL_DESC,  v.desc);
        item->setText(COL_UNIT,  sEmpty);
        item->setText(COL_VALUE, v.value);
        item->setText(COL_MIN,   v.min);
        item->setText(COL_MAX,   v.max);

        // Set checkeable item
        item->setFlags(item->flags()|Qt::ItemIsUserCheckable);
        item->setCheckState(COL_NAME,Qt::Unchecked);

        // Set item info
        item->setData(POS_DATA,Qt::UserRole,vs);
        item->setData(POS_VAR,Qt::UserRole,k);
    }
}

bool plotgraphs::loadData(QList<variable> vars, QList<QList<double>> values, QString hint)
{
    return loadGeneric(sEmpty,ffDAT,vars,values,0,hint);
}

bool plotgraphs::loadFile(QString file)
{
    unsigned             format;
    QList<variable>      vars;
    QList<double>        first,last;
    int                  num_mea;
    QString              error;
    QList<QList<double>> values;
    QStringList          varNames;
    //node                 nodeVar;
    int                  posInd = -1;

    // Read file vars
    if (checkInputFile(file,format,vars,first,last,num_mea,error) != FILE_OK)
    {
        QMessageBox::information(this,mOpenFile,mErrorFileVars);
        return false;
    }

    // Var name list
    for(int i=0;i<vars.count();i++)
    {
        varNames.append(vars[i].name);
        if (posInd == -1) posInd = indNames.indexOf(varNames[i].toLower()) > -1 ? i : -1;
    }

    // Read file data
    if (getDataFromFile(file,format,varNames,values,error) == FILE_UNSUPPORTED)
    {
        QMessageBox::information(this,mOpenFile,mErrorFileData);
        return false;
    }

    // Load data from file
    return loadGeneric(file,format,vars,values,posInd);
}

bool plotgraphs::loadGeneric(QString file, unsigned int format, QList<variable> vars, QList<QList<double>> values, int posInd, QString hint)
{
    node nodeVar;
    QList<QWidget *> lw;

    // Store info source
    nodeVar.file   = file;
    nodeVar.format = format;
    nodeVar.vars   = vars;
    nodeVar.values = values;
    nodeVar.posInd = posInd == -1 ? 0 : posInd;

    data.append(nodeVar);

    // Populate tree
    opt_project prj(ptNoType);
    QTreeWidgetItem *topItem;
    QString dataIndex     = QString::number(data.count()-1);
    QString dataIndexText = QString::number(data.count());
    QString topName       = (!hint.isEmpty() ? hint : format == ffDAT ? tData : tFile) + space + dataIndexText;

    topItem = new QTreeWidgetItem;
    topItem->setText(COL_NAME,topName);
    topItem->setToolTip(COL_NAME,!hint.isEmpty() ? hint : format == ffDAT ? tData : file);
    topItem->setStatusTip(COL_NAME,topItem->toolTip(COL_NAME));
    topItem->setIcon(COL_NAME, QIcon(format == ffCSV ? iCSV : format == ffDAT ? iData : iTRJ));
    ui->tree->addTopLevelItem(topItem);

    populateTreeGeneric(ui->tree,nodeVar.vars,prj,COL_NAME,dataIndex,setItemTreePlot,lw,topItem);

    // Menu
    ui->tree->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->tree,&QTreeWidget::customContextMenuRequested,this,&plotgraphs::treeMenu);

    return true;
}

void plotgraphs::treeMenu(const QPoint &pos)
{
    QTreeWidgetItem *item = ui->tree->itemAt(pos);

    if (item != NULL && !item->parent())
    {
        cmData.setProperty(REF_ITEM.toStdString().c_str(),qVariantFromValue((void *)item));
        cmData.exec(ui->tree->mapToGlobal(pos));
    }
}

void plotgraphs::on_tbVertical_clicked()
{
 ui->actionVertical->trigger();
}

void getSizes(QWidget *w, Qt::Orientation orientation, QList<int> &size)
{
    size.clear();
    for(int i=0;i<2;i++)
    {
        size.push_back(orientation == Qt::Orientation::Horizontal ?
                       w->height()/2 : w->width()/2);
    }
}

void plotgraphs::splitGraph(Qt::Orientation orientation)
{
    QWidget *w = NULL;

    if (focusItemGraph(w) && w->parentWidget()!=NULL)
    {
        QWidget              *parent   = w->parentWidget();
        QSplitter            *splitter = new QSplitter(orientation, this);
        QChartViewExt        *qcv      = new QChartViewExt;
        QList<int>            size,size2;

        splitter->setChildrenCollapsible(false);
        splitter->setHandleWidth(HANDLE_WIDTH);
        setDefaultChart(qcv);

        // Check window size is smaller than screen size

        // Screen size
        QRect sRec  = QApplication::desktop()->screenGeometry();
        int sHeight = sRec.height();
        int sWidth  = sRec.width();

        // Window minimum size
        int wHeight = minimumHeight();
        int wWidth  = minimumWidth();

        // New chart minimum size
        int cHeight = qcv->chart()->minimumHeight();
        int cWidth  = qcv->chart()->minimumWidth();

        // If new size is not bigger than screen size
        if (
            (orientation != Qt::Orientation::Vertical   || sHeight >= (wHeight + cHeight)) &&
            (orientation != Qt::Orientation::Horizontal || sWidth  >= (wWidth  + cWidth ))
           )
        {
            if (parent->inherits(cQSplitter.toStdString().c_str()))
            {
                QSplitter *qs = (QSplitter *)parent;
                QWidget   *w1,*w2;

                getSizes(qs,orientation,size);
                w1 = qs->widget(0);
                w2 = qs->widget(1);
                w1->setParent(NULL);
                w2->setParent(NULL);
                if (w == w1)
                {
                    getSizes(w1,orientation,size2);
                    qs->addWidget(splitter);
                    qs->addWidget(w2);

                }
                else
                {
                    getSizes(w2,orientation,size2);
                    qs->addWidget(w1);
                    qs->addWidget(splitter);
                }
                qs->setSizes(size);
                splitter->addWidget(w);
                splitter->addWidget(qcv);
                splitter->setSizes(size2);
                qcv->setFocus();
            }
            else
            {
                parent->layout()->removeWidget(w);
                splitter->addWidget(w);
                splitter->addWidget(qcv);
                getSizes(parent,orientation,size);
                splitter->setSizes(size);
                parent->layout()->addWidget(splitter);
                qcv->setFocus();
            }
        }
        else
        {
            // Delete dynamic objects
            delete splitter;
            delete qcv;
        }
    }
}

void plotgraphs::on_tbDeleteGraph_clicked()
{
    ui->actionRemoveGraph->trigger();
}

void plotgraphs::on_tbHoriozontal_clicked()
{
    ui->actionHorizontal->trigger();
}

int plotgraphs::addDefaultTab()
{
    QWidget *newTab = new QWidget(ui->tabWidget);
    QChartViewExt *qcv = new QChartViewExt;
    int tabIndex;

    setDefaultChart(qcv);
    newTab->setLayout(new QGridLayout());
    newTab->layout()->addWidget(qcv);
    tabIndex = ui->tabWidget->insertTab(ui->tabWidget->currentIndex()+1,newTab,QIcon(iIntegrator),
                                       tGraphs + space + QString::number(tabCounter));
    tabCounter++;
    return tabIndex;
}

void plotgraphs::on_tbAddTab_clicked()
{
    ui->tabWidget->setCurrentIndex(addDefaultTab());
}

void plotgraphs::on_tbDeleteTab_clicked()
{
    if (ui->tabWidget->count()>1)
        ui->tabWidget->removeTab(ui->tabWidget->currentIndex());
}

QLineSeries *plotgraphs::addSerie(QChartView *qcv, int posData, int posVar)
{
    QLineSeries *qls    = new QLineSeries;
    int          posInd = data[posData].posInd;

    double minT = std::numeric_limits<double>::max();
    double maxT = std::numeric_limits<double>::lowest();
    double minY = std::numeric_limits<double>::max();
    double maxY = std::numeric_limits<double>::lowest();

    // Get min and max values for current series
    for(int i=0;i<data[posData].values[posVar].count();i++)
    {
        qls->append(data[posData].values[posInd][i],
                    data[posData].values[posVar][i]);
        if (data[posData].values[posVar][i] < minY) minY = data[posData].values[posVar][i];
        if (data[posData].values[posVar][i] > maxY) maxY = data[posData].values[posVar][i];
        if (data[posData].values[posInd][i] < minT) minT = data[posData].values[posInd][i];
        if (data[posData].values[posInd][i] > maxT) maxT = data[posData].values[posInd][i];
    }

    // Set properties in series
    qls->setName(data[posData].vars[posVar].name);
    qls->setProperty(MIN_T.toStdString().c_str(),minT);
    qls->setProperty(MIN_Y.toStdString().c_str(),minY);
    qls->setProperty(MAX_T.toStdString().c_str(),maxT);
    qls->setProperty(MAX_Y.toStdString().c_str(),maxY);
    qls->installEventFilter(this);

    // Use OpenGL
    qls->setUseOpenGL(false);

    // Signals and slots
    // connect(qls, SIGNAL(clicked(QPointF)), this, SLOT(serie_click(QPointF)));
    connect(qls, SIGNAL(hovered(QPointF, bool)), this, SLOT(serie_hover(QPointF,bool)));

    // Add current series to chart
    qcv->chart()->addSeries(qls);

    // Set axes range
    setAxesRangeLine(qcv);

    return qls;
}

void plotgraphs::serie_hover(QPointF point, bool state)
{
    serieHover(point,state,sender(),&cmSerie);
}

void plotgraphs::deleteSerie(QChartView *qcv, QAbstractSeries *pSerie)
{
    qcv->chart()->removeSeries(pSerie);
    setAxesRangeLine(qcv);
    if (qcv->chart()->series().count()==0)
        setEmptyAxes(qcv);
}

void plotgraphs::deleteGraph(QChartView *qcv)
{
    while (qcv->chart()->series().count()>0)
            deleteSerie(qcv,qcv->chart()->series()[0]);
}

void plotgraphs::on_tree_itemChanged(QTreeWidgetItem *item, int column)
{
    // First column and changed the check state
    if (column == COL_NAME && item->data(column,Qt::CheckStateRole).isValid())
    {
        QWidget *w = NULL;

        // Focused widget is a QChartView
        if (focusItemGraph(w))
        {
            QChartView *qcv = (QChartView *)w;

            // Get item info for data and variable
            int posData = item->data(POS_DATA,Qt::UserRole).toInt();
            int posVar  = item->data(POS_VAR,Qt::UserRole).toInt();

            // Item checked
            if (item->checkState(column) == Qt::Checked)
            {
                // Draw serie
                QLineSeries *pSerie = addSerie(qcv,posData,posVar);
                // Set in serie variable position
                pSerie->setProperty(REF_DATA.toStdString().c_str(),item->data(POS_DATA,Qt::UserRole));
                pSerie->setProperty(REF_VAR.toStdString().c_str(),item->data(POS_VAR,Qt::UserRole));
                // Set in serie item reference
                pSerie->setProperty(REF_ITEM.toStdString().c_str(),qVariantFromValue((void *)item));
                // Set in item serie reference
                if (item->data(POS_SER,Qt::UserRole).isValid())
                {
                    QMap<QChartView *,QLineSeries *> *rser = (QMap<QChartView *,QLineSeries *> *)item->data(POS_SER,Qt::UserRole).value<void *>();
                    rser->insert(qcv,pSerie);
                    item->setData(POS_SER,Qt::UserRole, qVariantFromValue((void *)rser));
                }
                else
                {
                    QMap<QChartView *,QLineSeries *> *rser = new QMap<QChartView *,QLineSeries *>;
                    rser->insert(qcv,pSerie);
                    item->setData(POS_SER,Qt::UserRole, qVariantFromValue((void *)rser));
                }
            }
            // Item unchecked
            else
            {
                if (item->data(POS_SER,Qt::UserRole).isValid())
                {
                    // Get item serie reference
                    QMap<QChartView *,QLineSeries *> *rser = (QMap<QChartView *,QLineSeries *> *)item->data(POS_SER,Qt::UserRole).value<void *>();
                    QLineSeries *pSerie = rser->value(qcv);
                    // Delete serie
                    deleteSerie(qcv,pSerie);
                    // Remove from item
                    rser->remove(qcv);
                    item->setData(POS_SER,Qt::UserRole, qVariantFromValue((void *)rser));
                }
            }
        }
    }
}

void checkSubTree(QTreeWidgetItem *item, Qt::CheckState st)
{
  if (item->childCount() == 0)
      item->setCheckState(COL_NAME, st);
  for(int i=0;i<item->childCount();i++)
    checkSubTree(item->child(i), st);
}

void plotgraphs::UncheckTree()
{
    for(int i=0;i<ui->tree->topLevelItemCount();i++)
      checkSubTree(ui->tree->topLevelItem(i), Qt::Unchecked);
}

bool plotgraphs::eventFilter(QObject *object, QEvent *event)
{
    if ((event->type() == QEvent::FocusIn) && object->inherits(cQChartView.toStdString().c_str()))
    {
        QChartView *qcv = (QChartView *)object;

        // Block signals in tree
        bool oldState = ui->tree->blockSignals(true);

        // Uncheck all items
        UncheckTree();

        // Get item pointers from series and check them
        for(int i=0;i<qcv->chart()->series().count();i++)
        {
            QAbstractSeries *s = qcv->chart()->series()[i];

            if (s->property(REF_ITEM.toStdString().c_str()).isValid())
            {
                QTreeWidgetItem *item = (QTreeWidgetItem *)s->property(REF_ITEM.toStdString().c_str()).value<void *>();
                item->setCheckState(COL_NAME,Qt::Checked);
            }
        }    

        // Restore signals in tree
        ui->tree->blockSignals(oldState);

        return true;
    }

    return false;
}

void plotgraphs::on_tbClearGraph_clicked()
{
    ui->actionClear->trigger();
}

void plotgraphs::onCustomContextMenu(const QPoint &p)
{
    QWidget *w = NULL;

    if (focusItemGraph(w))
    {
        QChartView  *qcv = ((QChartView *)w);

        if (selectedSerie())
            cmSerie.exec(qcv->mapToGlobal(p));
        else
            cmGraph.exec(qcv->mapToGlobal(p));
    }
}

void plotgraphs::on_tbZoomReset_clicked()
{
    ui->actionZoomReset->trigger();
}

void plotgraphs::on_tbZoomOut_clicked()
{
    ui->actionZoomOut->trigger();
}

void plotgraphs::on_actionHorizontal_triggered()
{
    splitGraph(Qt::Horizontal);
}

void plotgraphs::on_actionClear_triggered()
{
    QWidget *w = NULL;

    if (focusItemGraph(w))
    {
        QChart *c = ((QChartView *)w)->chart();

        UncheckTree();
        c->setTitle(sEmpty);
        c->legend()->setVisible(false);
        c->setTheme(QChart::ChartTheme::ChartThemeLight);
    }
}

void plotgraphs::on_actionRemoveGraph_triggered()
{
    QWidget *w = NULL;

    if (focusItemGraph(w) && w->parentWidget()!=NULL &&
        w->parentWidget()->inherits(cQSplitter.toStdString().c_str()))
    {
        if (w->parentWidget()->parentWidget() != NULL &&
            w->parentWidget()->parentWidget()->inherits(cQSplitter.toStdString().c_str()))
        {
            QSplitter *qs  = (QSplitter *)w->parentWidget();
            QSplitter *qsp = (QSplitter *)qs->parentWidget();
            QWidget   *ws  = w == qs->widget(0) ? qs->widget(1) : qs->widget(0);
            QWidget   *w1,*w2;

            w1 = qsp->widget(0);
            w2 = qsp->widget(1);
            ws->setParent(NULL);
            w1->setParent(NULL);
            w2->setParent(NULL);
            if (qs == w1)
            {
                qsp->addWidget(ws);
                qsp->addWidget(w2);
            }
            else
            {
                qsp->addWidget(w1);
                qsp->addWidget(ws);
            }
            delete w;
            delete qs;
            ws->setFocus();
        }
        else
        {
            QSplitter *qs  = (QSplitter *)w->parentWidget();
            QWidget   *qsp = qs->parentWidget();
            QWidget   *ws  = w == qs->widget(0) ? qs->widget(1) : qs->widget(0);

            ws->setParent(NULL);
            qsp->layout()->removeWidget(qs);
            qsp->layout()->addWidget(ws);
            delete w;
            delete qs;
            ws->setFocus();
        }
        removedGraph = true;
    }
    removedGraph = false;
}

void plotgraphs::on_actionZoomReset_triggered()
{
    QWidget *w = NULL;

    if (focusItemGraph(w))
        ((QChartView *)w)->chart()->zoomReset();
}

void plotgraphs::on_actionZoomOut_triggered()
{
    QWidget *w = NULL;

    if (focusItemGraph(w))
        ((QChartView *)w)->chart()->zoomOut();
}

void plotgraphs::on_actionVertical_triggered()
{
    splitGraph(Qt::Vertical);
}

void plotgraphs::on_actionRemoveSerie_triggered()
{
    // Get serie
    QLineSeries *pSerie = (QLineSeries *)cmSerie.property(REF_SER.toStdString().c_str()).value<void *>();
    // Get item from serie
    QTreeWidgetItem *item = (QTreeWidgetItem *)pSerie->property(REF_ITEM.toStdString().c_str()).value<void *>();
    // Uncheck item
    item->setCheckState(COL_NAME,Qt::Unchecked);
}

void plotgraphs::on_actionConfigureSerie_triggered()
{
    seriesConfig *sc = new seriesConfig;

    // Get serie
    QLineSeries *pSerie = (QLineSeries *)cmSerie.property(REF_SER.toStdString().c_str()).value<void *>();

    sc->setPen(pSerie->pen());
    sc->setOpacity(pSerie->opacity());
    sc->setName(pSerie->name());
    sc->setPoints(pSerie->pointsVisible());
    sc->setPointsLabel(pSerie->pointLabelsVisible());
    sc->setPointsColor(pSerie->pointLabelsColor());
    sc->setPointsFont(pSerie->pointLabelsFont());
    sc->setPointsFormat(pSerie->pointLabelsFormat());
    sc->setPointsClippping(pSerie->pointLabelsClipping());
    // Style
    if (!styleDialogs.isEmpty())
    {
        sc->deleteStyleSheet();
        sc->setStyleSheet(styleDialogs);
    }
    //Exec
    if (sc->exec())
    {
        pSerie->setOpacity(sc->getOpacity());
        pSerie->setPen(sc->getPen());
        pSerie->setName(sc->getName());
        pSerie->setPointsVisible(sc->getPoints());
        pSerie->setPointLabelsVisible(sc->getPointsLabel());
        pSerie->setPointLabelsColor(sc->getPointsColor());
        pSerie->setPointLabelsFont(sc->getPointsFont());
        pSerie->setPointLabelsFormat(sc->getPointsFormat());
        pSerie->setPointLabelsClipping(sc->getPointsClipping());
    }
    delete sc;
}

void plotgraphs::setChartTheme(QChart::ChartTheme t)
{
    QWidget *w = NULL;

    if (focusItemGraph(w))
        ((QChartView *)w)->chart()->setTheme(t);
}

void plotgraphs::on_actionLight_triggered()
{
    setChartTheme(QChart::ChartThemeLight);
}

void plotgraphs::on_actionBlue_cerulean_triggered()
{
    setChartTheme(QChart::ChartThemeBlueCerulean);
}

void plotgraphs::on_actionDark_triggered()
{
    setChartTheme(QChart::ChartThemeDark);
}

void plotgraphs::on_actionBorwn_sand_triggered()
{
    setChartTheme(QChart::ChartThemeBrownSand);
}

void plotgraphs::on_actionBlue_NCS_triggered()
{
    setChartTheme(QChart::ChartThemeBlueNcs);
}

void plotgraphs::on_actionHigh_contrast_triggered()
{
    setChartTheme(QChart::ChartThemeHighContrast);
}

void plotgraphs::on_actionBlue_icy_triggered()
{
    setChartTheme(QChart::ChartThemeBlueIcy);
}

void plotgraphs::on_actionQt_triggered()
{
    setChartTheme(QChart::ChartThemeQt);
}

void plotgraphs::on_actionConfigureGraph_triggered()
{

    QWidget *w = NULL;

    if (focusItemGraph(w))
    {
        QChartView *cv    = (QChartView *)w;
        QChart     *chart = cv->chart();
        graphConfig gc;

        // Title
        gc.setTitleBrush(chart->titleBrush());
        gc.setTitleFont(chart->titleFont());
        gc.setTitleText(chart->title());
        // Legend
        gc.setLegendVisible(chart->legend()->isVisible());
        gc.setBackgroundVisible(chart->legend()->isBackgroundVisible());
        gc.setLabelFontColor(chart->legend()->labelColor());
        gc.setBackgroundPen(chart->legend()->pen());
        gc.setLegendFont(chart->legend()->font());
        gc.setBackgroundBrush(chart->legend()->brush());
        gc.setAlignment(chart->legend()->alignment());
        gc.setAttached(chart->legend()->isAttachedToChart());
        gc.setX(chart->legend()->geometry().top(),chart->legend()->geometry().height(),chart->geometry().height());
        gc.setY(chart->legend()->geometry().left(),chart->legend()->geometry().width(),chart->geometry().width());
        // Style
        if (!styleDialogs.isEmpty())
        {
            gc.deleteStyleSheet();
            gc.setStyleSheet(styleDialogs);
        }
        // Exec
        if (gc.exec())
        {
            // Title
            chart->setTitle(gc.getTitleText());
            chart->setTitleFont(gc.getTitleFont());
            chart->setTitleBrush(gc.getTitleBrush());
            // Legend
            chart->legend()->setVisible(gc.getLegedVisible());
            chart->legend()->setBackgroundVisible(gc.getBackgroundVisible());
            chart->legend()->setLabelColor(gc.getLabelFontColor());
            chart->legend()->setPen(gc.getBackgroundPen());
            chart->legend()->setFont(gc.getLegendFont());
            chart->legend()->setBrush(gc.getBackgroundBrush());
            if (gc.getAttached())
            {
                chart->legend()->attachToChart();
                chart->legend()->setAlignment(gc.getAlignment());
            }
            else
            {
                QRectF geo = chart->legend()->geometry();

                chart->legend()->detachFromChart();
                geo.setTop(gc.getX1());
                geo.setLeft(gc.getY1());
                geo.setHeight(gc.getX2());
                geo.setWidth(gc.getY2());
                chart->legend()->setGeometry(geo);
            }
            // Axis captions
            QValueAxis *ax = (QValueAxis *)chart->axes(Qt::Horizontal)[0];
            QValueAxis *ay = (QValueAxis *)chart->axes(Qt::Vertical)[0];
            ax->setTitleText(gc.getXcaption());
            ay->setTitleText(gc.getYcaption());
        }
    }
}

QString plotgraphs::getStyleDialogs() const
{
    return styleDialogs;
}

void plotgraphs::setStyleDialogs(const QString &value)
{
    styleDialogs = value;
}

void plotgraphs::on_actionChartFileImage_triggered()
{
    QWidget *w = NULL;

    if (focusItemGraph(w))
    {
        QChartView  *qcv = ((QChartView *)w);
        chartToImageFile(this,qcv);
    }
}

void plotgraphs::on_actionChartFileText_triggered()
{
    QWidget *w = NULL;

    if (focusItemGraph(w))
    {
        QList<QString>       names;
        QList<double>        time;
        QList<QList<double>> values;

        QChartView  *qcv = ((QChartView *)w);
        chartToData(qcv,time,values,names);
        copyVarsFile(this,time,values,names);
    }
}

void plotgraphs::on_actionChartClipboardImage_triggered()
{
    QWidget *w = NULL;

    if (focusItemGraph(w))
    {
        QChartView  *qcv = ((QChartView *)w);
        chartToImage(qcv);
    }
}

void plotgraphs::on_actionChartClipboardText_triggered()
{
    QWidget *w = NULL;

    if (focusItemGraph(w))
    {
        QList<QString>       names;
        QList<double>        time;
        QList<QList<double>> values;

        QChartView  *qcv = ((QChartView *)w);
        chartToData(qcv,time,values,names);
        copyVarsClipboard(time,values,names);
    }
}

void plotgraphs::on_actionSerieClipboardText_triggered()
{
    // Get serie
    QList<double>        time;
    QList<QList<double>> values;
    QStringList          names;
    QList<double>        v;
    QString              n;
    QLineSeries *pSerie = (QLineSeries *)cmSerie.property(REF_SER.toStdString().c_str()).value<void *>();

    serieToData(pSerie,time,v,n);
    values.append(v);
    names.append("Time");
    names.append(n);
    copyVarsClipboard(time,values,names);
}

void plotgraphs::on_actionSerieFileText_triggered()
{
    // Get serie
    QList<double>        time;
    QList<QList<double>> values;
    QStringList          names;
    QList<double>        v;
    QString              n;
    QLineSeries *pSerie = (QLineSeries *)cmSerie.property(REF_SER.toStdString().c_str()).value<void *>();

    serieToData(pSerie,time,v,n);
    values.append(v);
    names.append("Time");
    names.append(n);
    copyVarsFile(this,time,values,names);
}

void plotgraphs::on_actionRemoveData_triggered()
{
    if (cmData.property(REF_ITEM.toStdString().c_str()).isValid())
    {
        QTreeWidgetItem *item    = (QTreeWidgetItem *)cmData.property(REF_ITEM.toStdString().c_str()).value<void *>();
        //int              posData = item->data(POS_DATA,Qt::UserRole).toInt();

        delete item;
        // -------
        // WARNING: Data cannot be removed because position in list are stored.
        // -------
    }
}

void plotgraphs::on_actionDataFileText_triggered()
{
    if (cmData.property(REF_ITEM.toStdString().c_str()).isValid())
    {
        // Item and data position
        QTreeWidgetItem *item    = (QTreeWidgetItem *)cmData.property(REF_ITEM.toStdString().c_str()).value<void *>();
        int              posData = item->data(POS_DATA,Qt::UserRole).toInt();

        // Info to be saved
        QList<QString>       names;
        QList<double>        time;
        QList<QList<double>> values;

        // Set names
        for(int i=0;i<data[posData].vars.count();i++)
            names.append(data[posData].vars[i].name);

        // Set time
        time.append(data[posData].values[0]);

        // Set values
        data[posData].values.removeFirst();
        values.append(data[posData].values);

        // Save to file
        copyVarsFile(this,time,values,names);
    }
}
