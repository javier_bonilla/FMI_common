#include "graphconfig.h"
#include "ui_graphconfig.h"

#include "commonapp.h"
#include "brushconf.h"
#include "penconfig.h"

graphConfig::graphConfig(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::graphConfig)
{
    ui->setupUi(this);
    ui->tabWidget->setCurrentIndex(0);
    checkEnable();

    ui->tabWidget->removeTab(ui->tabWidget->count()-1);
    ui->tabWidget->removeTab(ui->tabWidget->count()-1);

    //ui->tabWidget->removeTab(ui->tabWidget->count()-1);
    //ui->tabWidget->removeTab(ui->tabWidget->count()-1);
}

graphConfig::~graphConfig()
{
    delete ui;
}

void graphConfig::deleteStyleSheet()
{
    ui->lTitle->setStyleSheet(sEmpty);
}

QBrush graphConfig::getTitleBrush() const
{
    return titleBrush;
}

QFont graphConfig::getTitleFont() const
{
    return titleFont;
}

QString graphConfig::getTitleText() const
{
    return titleText;
}

void graphConfig::setTitleBrush(const QBrush &value)
{
    titleBrush = value;
    setBackgroundColor(ui->leTitleColor, titleBrush.color());
}

void graphConfig::setTitleFont(const QFont &value)
{
    titleFont = value;
    ui->leTitleFont->setText(fontToString(titleFont));
}

void graphConfig::setTitleText(const QString &value)
{
    titleText = value;
    ui->lTitleGraph->setText(titleText);
}

void graphConfig::on_lTitleGraph_textChanged(const QString &arg1)
{
    titleText = arg1;
}

bool graphConfig::getLegedVisible()
{
    return ui->chkLegendVisible->isChecked();
}

void graphConfig::setLegendVisible(bool v)
{
    ui->chkLegendVisible->setChecked(v);
}

bool graphConfig::getBackgroundVisible()
{
    return ui->chkBackground->isChecked();
}

void graphConfig::setBackgroundVisible(bool v)
{
    ui->chkBackground->setChecked(v);
}

QColor graphConfig::getLabelFontColor()
{
    return cLabelFontColor;
}

void graphConfig::setLabelFontColor(QColor c)
{
    cLabelFontColor = c;
    setBackgroundColor(ui->leLabelFontColor, cLabelFontColor);
}

void graphConfig::on_tbTitleFont_clicked()
{
    dFont.setCurrentFont(titleFont);
    dFont.setWindowIcon(windowIcon());
    if (dFont.exec())
        setTitleFont(dFont.currentFont());
}

void graphConfig::on_tbTitleColor_clicked()
{
    dColor.setCurrentColor(titleBrush.color());
    dColor.setWindowIcon(windowIcon());
    if (dColor.exec())
        setTitleBrush(dColor.currentColor());
}
void graphConfig::on_tbLabelFontColor_clicked()
{
    dColor.setCurrentColor(cLabelFontColor);
    dColor.setWindowIcon(windowIcon());
    if (dColor.exec())
        setLabelFontColor(dColor.currentColor());
}

void graphConfig::on_tbBorder_clicked()
{
    penConfig p;

    p.setWindowIcon(windowIcon());
    if (!styleSheet().isEmpty())
    {
        p.deleteSytyleSheet();
        p.setStyleSheet(styleSheet());
    }
    p.setPen(pBorder);
    if (p.exec())
        setBackgroundPen(p.getPen());
}

void graphConfig::setBackgroundPen(QPen p)
{
    pBorder = p;
    setLineStyleLabel(ui->leBorder,pBorder);
}

QPen graphConfig::getBackgroundPen()
{
    return pBorder;
}

void graphConfig::on_tbLabelFont_clicked()
{
    dFont.setCurrentFont(fLegend);
    dFont.setWindowIcon(windowIcon());
    if (dFont.exec())
        setLegendFont(dFont.currentFont());
}

void graphConfig::setLegendFont(QFont f)
{
    fLegend = f;
    ui->leLabelFont->setText(fontToString(fLegend));
}

QFont graphConfig::getLegendFont()
{
    return fLegend;
}

void graphConfig::on_tbBackground_clicked()
{
    brushConf bc;

    bc.setWindowIcon(windowIcon());
    if (!styleSheet().isEmpty())
    {
        qDebug() << styleSheet();
        bc.deleteSytyleSheet();
        bc.setStyleSheet(styleSheet());
    }
    bc.setBrush(bBackground);
    if (bc.exec())
        setBackgroundBrush(bc.getBrush());
}

void graphConfig::setBackgroundBrush(QBrush b)
{
    bBackground = b;
    setBrushStyleLabel(ui->leBackgroundColor,bBackground);

}

QBrush graphConfig::getBackgroundBrush()
{
    return bBackground;
}

void graphConfig::on_chkLegendVisible_stateChanged(int arg1)
{
    Q_UNUSED(arg1);

    checkEnable();
}
void graphConfig::on_cbAlign_currentIndexChanged(int index)
{
    Q_UNUSED(index);

    checkEnable();
}

void graphConfig::checkEnable()
{
    ui->chkBackground->setEnabled(ui->chkLegendVisible->isChecked());
    ui->tbBackground->setEnabled(ui->chkLegendVisible->isChecked());
    ui->tbBorder->setEnabled(ui->chkLegendVisible->isChecked());
    ui->tbLabelFontColor->setEnabled(ui->chkLegendVisible->isChecked());
    ui->tbLabelFont->setEnabled(ui->chkLegendVisible->isChecked());
    ui->sbX1->setEnabled(ui->chkLegendVisible->isChecked() && ui->cbAlign->currentIndex() == GRAPH_ALIGN_CUSTOM);
    ui->sbX2->setEnabled(ui->chkLegendVisible->isChecked() && ui->cbAlign->currentIndex() == GRAPH_ALIGN_CUSTOM);
    ui->sbY1->setEnabled(ui->chkLegendVisible->isChecked() && ui->cbAlign->currentIndex() == GRAPH_ALIGN_CUSTOM);
    ui->sbY2->setEnabled(ui->chkLegendVisible->isChecked() && ui->cbAlign->currentIndex() == GRAPH_ALIGN_CUSTOM);
    ui->cbAlign->setEnabled(ui->chkLegendVisible->isChecked());
}

void graphConfig::setX(int x1, int x2, int xMax)
{
    ui->sbX1->setMinimum(0);
    ui->sbX1->setMaximum(xMax);
    ui->sbX1->setSuffix(" / " + QString::number(xMax));
    ui->sbX1->setValue(x1);

    ui->sbX2->setMinimum(0);
    ui->sbX2->setMaximum(xMax);
    ui->sbX2->setSuffix(" / " + QString::number(xMax));
    ui->sbX2->setValue(x2);
}

void graphConfig::setY(int y1, int y2, int yMax)
{
    ui->sbY1->setMinimum(0);
    ui->sbY1->setMaximum(yMax);
    ui->sbY1->setSuffix(" / " + QString::number(yMax));
    ui->sbY1->setValue(y1);

    ui->sbY2->setMinimum(0);
    ui->sbY2->setMaximum(yMax);
    ui->sbY2->setSuffix(" / " + QString::number(yMax));
    ui->sbY2->setValue(y2);
}

int graphConfig::getX1()
{
    return ui->sbX1->value();
}

int graphConfig::getY1()
{
    return ui->sbY1->value();
}

int graphConfig::getX2()
{
    return ui->sbX2->value();
}

int graphConfig::getY2()
{
    return ui->sbY2->value();
}

void graphConfig::setAlignment(Qt::Alignment a)
{
    if      (a == Qt::AlignTop)    ui->cbAlign->setCurrentIndex(GRAPH_ALIGN_TOP);
    else if (a == Qt::AlignBottom) ui->cbAlign->setCurrentIndex(GRAPH_ALIGN_BOTTOM);
    else if (a == Qt::AlignLeft)   ui->cbAlign->setCurrentIndex(GRAPH_ALIGN_LEFT);
    else if (a == Qt::AlignRight)  ui->cbAlign->setCurrentIndex(GRAPH_ALIGN_RIGHT);
    checkEnable();
}

void graphConfig::setAttached(bool a)
{
    if (!a) ui->cbAlign->setCurrentIndex(GRAPH_ALIGN_CUSTOM);
    checkEnable();
}

Qt::Alignment graphConfig::getAlignment()
{
    switch(ui->cbAlign->currentIndex())
    {
        case GRAPH_ALIGN_TOP     : return Qt::AlignTop;      break;
        case GRAPH_ALIGN_BOTTOM  : return Qt::AlignBottom;   break;
        case GRAPH_ALIGN_LEFT    : return Qt::AlignLeft;     break;
        case GRAPH_ALIGN_RIGHT   : return Qt::AlignRight;    break;
        default                  : return Qt::AlignAbsolute; break;
    }
}

bool graphConfig::getAttached()
{
    return ui->cbAlign->currentIndex() != GRAPH_ALIGN_CUSTOM;
}

QString graphConfig::getXcaption()
{
    return ui->x_caption->text();
}

QString graphConfig::getYcaption()
{
    return ui->y_caption->text();
}
