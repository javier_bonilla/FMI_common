#ifndef SERIESCONFIG_H
#define SERIESCONFIG_H

#include <QDialog>
#include <QColorDialog>
#include <QFontDialog>
#include <QPen>
#include <QComboBox>

namespace Ui {
class seriesConfig;
}

class seriesConfig : public QDialog
{
    Q_OBJECT

public:
    explicit seriesConfig(QWidget *parent = 0);
    ~seriesConfig();

    // Set info
    void setPen(QPen p);
    void setOpacity(double o);
    void setName(QString n);
    void setPoints(bool b);
    void setPointsLabel(bool b);
    void setPointsColor(QColor c);
    void setPointsFont(QFont f);
    void setPointsFormat(QString f);
    void setPointsClippping(bool b);
    void deleteStyleSheet();

    // Get info
    QPen    getPen();
    QString getName();
    bool    getPoints();
    bool    getPointsLabel();
    QColor  getPointsColor();
    QString getPointsFormat();
    double  getOpacity();
    QFont   getPointsFont();
    bool    getPointsClipping();

private slots:
    void on_tbColor_clicked();
    void on_tbColorPoint_clicked();
    void on_tbFont_clicked();
    void on_cbLine_currentIndexChanged(const QString &arg1);
    void on_sbWidth_valueChanged(int arg1);

private:
    Ui::seriesConfig *ui;
    QPen   pen;
    QFont  pointFont;
    QColor pointColor;

    QColorDialog dColor;
    QFontDialog  dFont;

    void setLineTextColor();
    void setPointTextColor();
    void setFontText();
};

#endif // SERIESCONFIG_H
