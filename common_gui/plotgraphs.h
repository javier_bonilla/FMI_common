#ifndef PLOTGRAPHS_H
#define PLOTGRAPHS_H

#include <QMainWindow>
#include <QtCharts>

using namespace QtCharts;

#include "commonapp.h"

namespace Ui {
class plotgraphs;
}

class node
{
public:
    QString              file;
    unsigned             format;
    QList<variable>      vars;
    QList<QList<double>> values;
    unsigned             posInd;
};

class plotgraphs : public QMainWindow
{
    Q_OBJECT

public:
    explicit plotgraphs(QWidget *parent = 0);
    ~plotgraphs();

    bool loadFile(QString file);
    bool loadData(QList<variable> vars, QList<QList<double>> values, QString hint = "");
    bool loadGeneric(QString file, unsigned int format, QList<variable> vars, QList<QList<double>> values, int posInd, QString hint = "");
    void clearAll();
    void splitGraph(Qt::Orientation orientation);
    int addDefaultTab();
    QLineSeries *addSerie(QChartView *qcv, int posData, int posVar);
    void deleteSerie(QChartView *qcv, QAbstractSeries *pSerie);
    void deleteGraph(QChartView *qcv);
    void setDefaultChart(QtCharts::QChartView *qcv);
    bool eventFilter(QObject *object, QEvent *event);
    void UncheckTree();
    void setChartTheme(QChart::ChartTheme t);
    QString getStyleDialogs() const;
    void setStyleDialogs(const QString &value);

private slots:
    void on_tbVertical_clicked();
    void on_tbHoriozontal_clicked();
    void on_tbAddTab_clicked();
    void on_tbDeleteTab_clicked();
    void on_tbDeleteGraph_clicked();
    void on_tree_itemChanged(QTreeWidgetItem *item, int column);
    void on_tbClearGraph_clicked();
    //void serie_click(QPointF point);
    void serie_hover(QPointF point, bool state);
    void onCustomContextMenu(const QPoint &p);
    void on_tbZoomReset_clicked();
    void on_tbZoomOut_clicked();
    void on_actionHorizontal_triggered();
    void on_actionClear_triggered();
    void on_actionRemoveGraph_triggered();
    void on_actionZoomReset_triggered();
    void on_actionZoomOut_triggered();
    void on_actionVertical_triggered();
    void on_actionRemoveSerie_triggered();
    void on_actionConfigureSerie_triggered();
    void on_actionLight_triggered();
    void on_actionBlue_cerulean_triggered();
    void on_actionDark_triggered();
    void on_actionBorwn_sand_triggered();
    void on_actionBlue_NCS_triggered();
    void on_actionHigh_contrast_triggered();
    void on_actionBlue_icy_triggered();
    void on_actionQt_triggered();
    void on_actionConfigureGraph_triggered();
    void on_actionChartFileImage_triggered();
    void on_actionChartFileText_triggered();
    void on_actionChartClipboardImage_triggered();
    void on_actionChartClipboardText_triggered();
    void on_actionSerieClipboardText_triggered();
    void on_actionSerieFileText_triggered();
    void treeMenu(const QPoint &pos);

    void on_actionRemoveData_triggered();

    void on_actionDataFileText_triggered();

private:
    Ui::plotgraphs *ui;
    QList<node> data;
    QMenu cmGraph;
    QMenu cmSerie;
    QMenu cmClipboardGraph;
    QMenu cmSaveGraph;
    QMenu cmData;
    int tabCounter;
    QMenu *mStyle;
    QString styleDialogs;

    bool removedGraph; // Flag for delete graph
};

#endif // PLOTGRAPHS_H
