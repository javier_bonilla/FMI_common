#include "integrator.h"
#include "ui_integrator.h"

//#include "ui_conf.h"
#include <QDoubleValidator>
#include <QPushButton>
#include <QSettings>
#include "commonapp.h"
//#include "confapp.h"
#include "common.h"

const double ORDER_MIN     = 1;
const double ORDER_MAX     = 100;
const double STEP_SIZE_MIN = 1e-300;
const double STEP_SIZE_MAX = 1e+300;
const double ABS_TOL_MIN   = 1e-300;
const double ABS_TOL_MAX   = 1e+300;
const double REL_TOL_MIN   = 1e-300;
const double REL_TOL_MAX   = 1e+300;

// Buttons space
const int spaceSetDef     = 60;
const int spaceRestoreDef = 20;

integrator::integrator(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::integrator)
{
    ui->setupUi(this);
    readGeometry(company,appName,this,intWin);

    // Label texts
    setWindowTitle(tNumIntConf);
    ui->lTitle->setText(tNumInt2);
    ui->lbiIntegrator->setText(tNumInt);
    ui->lbiOrder->setText(tOrder);
    ui->lbiStepSize->setText(tStepSize);
    ui->lbiAbsTol->setText(tAbsTol);
    ui->lbiRelTol->setText(tRelTol);
    ui->lbDesc->setText(tDesc2);

    // Double validators
    ui->leOrder->setValidator(new QDoubleValidator(ORDER_MIN,ORDER_MAX,NUM_DEC,this));
    ui->leStepSize->setValidator(new QDoubleValidator(STEP_SIZE_MIN,STEP_SIZE_MAX,NUM_DEC,this));
    ui->leAbsTol->setValidator(new QDoubleValidator(ABS_TOL_MIN,ABS_TOL_MAX,NUM_DEC,this));
    ui->leRelTol->setValidator(new QDoubleValidator(REL_TOL_MIN,REL_TOL_MAX,NUM_DEC,this));

    // External links
    ui->teDesc->setOpenExternalLinks(true);

    // Populate integrator info
    populateIntegratorInfo();

    // Button box
    ui->buttonBox->button(QDialogButtonBox::Apply)->setText(tSetDef);
    QRect geo = ui->buttonBox->button(QDialogButtonBox::Apply)->geometry();
    geo.setWidth(geo.width()+spaceSetDef);
    ui->buttonBox->button(QDialogButtonBox::Apply)->setGeometry(geo);

    ui->buttonBox->button(QDialogButtonBox::RestoreDefaults)->setText(tRestoreDef);
    geo = ui->buttonBox->button(QDialogButtonBox::RestoreDefaults)->geometry();
    geo.setWidth(geo.width()+spaceRestoreDef);
    ui->buttonBox->button(QDialogButtonBox::RestoreDefaults)->setGeometry(geo);
}

void integrator::hideDefaultsButtons()
{
    ui->buttonBox->removeButton(ui->buttonBox->button(QDialogButtonBox::RestoreDefaults));
    ui->buttonBox->removeButton(ui->buttonBox->button(QDialogButtonBox::Apply));
}

void integrator::populateIntegratorInfo()
{

    const QString htmlHeader   = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\"><html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">p, li { white-space: pre-wrap; }</style></head><body style=\" font-family:'Droid Sans'; font-size:9pt; font-weight:400; font-style:normal;\">";
    const QString odeint_url   = "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><a href=\"http://headmyshoulder.github.io/odeint-v2/\"><span style=\" text-decoration: underline; color:#2980b9;\">ODEINT library</span></a></p>";
    const QString sundials_url = "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><a href=\"http://computation.llnl.gov/projects/sundials\"><span style=\" text-decoration: underline; color:#2980b9;\">SUNDIALS library</span></a></p>";

    int_name.append(integratorToText(IntegratorType::eu));   int_key.append(IntegratorType::eu);   int_step.append(true);  int_order.append(false); int_tols.append(false); int_des.append(htmlHeader+"Forward Euler method.<hr><hr>" + odeint_url + "</body></html>");
    int_name.append(integratorToText(IntegratorType::rk));   int_key.append(IntegratorType::rk);   int_step.append(true);  int_order.append(false); int_tols.append(false); int_des.append(htmlHeader+"4th order Runge-Kutta method with constant step size.<hr><hr>" + odeint_url+ "</body></html>");
    int_name.append(integratorToText(IntegratorType::abm));  int_key.append(IntegratorType::abm);  int_step.append(true);  int_order.append(true);  int_tols.append(false); int_des.append(htmlHeader+"Adams-Bashforth-Moulton multistep method with adjustable order and constant step size.<hr><hr>" + odeint_url+ "</body></html>");
    int_name.append(integratorToText(IntegratorType::ck));   int_key.append(IntegratorType::ck);   int_step.append(false); int_order.append(false); int_tols.append(true);  int_des.append(htmlHeader+"5th order Runge-Kutta-Cash-Karp method with controlled step size.<hr><hr>" + odeint_url+ "</body></html>");
    int_name.append(integratorToText(IntegratorType::dp));   int_key.append(IntegratorType::dp);   int_step.append(false); int_order.append(false); int_tols.append(true);  int_des.append(htmlHeader+"5th order Runge-Kutta-Dormand-Prince method with controlled step size.<hr><hr>" + odeint_url+ "</body></html>");
    int_name.append(integratorToText(IntegratorType::fe));   int_key.append(IntegratorType::fe);   int_step.append(false); int_order.append(false); int_tols.append(true);  int_des.append(htmlHeader+"8th order Runge-Kutta-Fehlberg method with controlled step size.<hr><hr>" + odeint_url+ "</body></html>");
    int_name.append(integratorToText(IntegratorType::bs));   int_key.append(IntegratorType::bs);   int_step.append(false); int_order.append(false); int_tols.append(true);  int_des.append(htmlHeader+"Bulirsch-Stoer method with controlled step size.<hr><hr>" + odeint_url+ "</body></html>");
    int_name.append(integratorToText(IntegratorType::ro));   int_key.append(IntegratorType::ro);   int_step.append(false); int_order.append(false); int_tols.append(true);  int_des.append(htmlHeader+"4th Rosenbrock Method for stiff problems.<hr><hr>" + odeint_url+ "</body></html>");
    int_name.append(integratorToText(IntegratorType::bdf));  int_key.append(IntegratorType::bdf);  int_step.append(false); int_order.append(false); int_tols.append(true);  int_des.append(htmlHeader+"Backwards Differentiation formula from Sundials. This stepper has adaptive step size, error control and an internal algorithm for the event search loop. The order varies between 1 and 5. Well suited for stiff problems.<hr><hr>" + sundials_url+ "</body></html>");
    int_name.append(integratorToText(IntegratorType::abm2)); int_key.append(IntegratorType::abm2); int_step.append(false); int_order.append(false); int_tols.append(true);  int_des.append(htmlHeader+"Adams bashforth moulton method from sundials. This stepper has adaptive step size, error control, and an internal algorithm for the event search loop. The order varies between 1 and 12. Well suited for smooth problems.<hr><hr>" + sundials_url+ "</body></html>");


    for (int i=0;i<int_name.size();i++)
        ui->cbIntegrator->addItem(int_name[i],int_key[i]);
}

integrator::~integrator()
{
    writeGeometry(company,appName,this,intWin);
    delete ui;
}

void integrator::deleteStyleSheet()
{
    ui->lTitle->setStyleSheet(sEmpty);
}

void integrator::on_cbIntegrator_currentIndexChanged(int index)
{
    if (index < int_name.size())
    {
        ui->leOrder->setEnabled(int_order[index]);
        ui->leStepSize->setEnabled(int_step[index]);
        ui->leAbsTol->setEnabled(int_tols[index]);
        ui->leRelTol->setEnabled(int_tols[index]);
        ui->teDesc->setText(int_des[index]);
    }
    else
    {
        ui->leOrder->setEnabled(false);
        ui->leStepSize->setEnabled(false);
        ui->leAbsTol->setEnabled(false);
        ui->leRelTol->setEnabled(false);
        ui->teDesc->setText(sEmpty);
    }
}

int integrator::getIntegratorIndex(IntegratorType key)
{
    int  i     = 0;
    bool found = false;

    do{
        found = (int_key[i] == key);
        if (!found) i++;
    }while(!found && i<int_key.size());

    return (found ? i : 0);
}

void integrator::on_buttonBox_clicked(QAbstractButton *button)
{
    if (button == ui->buttonBox->button(QDialogButtonBox::Apply))
    {
        QSettings settings(company,appName);

        settings.setValue(stOrder,ui->leOrder->text());
        settings.setValue(stStepSize,ui->leStepSize->text());
        settings.setValue(stAbsTol,ui->leAbsTol->text());
        settings.setValue(stRelTol,ui->leRelTol->text());
        settings.setValue(stInt,ui->cbIntegrator->currentData().toUInt());
    }
    else if (button == ui->buttonBox->button(QDialogButtonBox::RestoreDefaults))
    {
        setIntInfo(sim_restoreFromDefaults());
    }
    else if (button == ui->buttonBox->button(QDialogButtonBox::Cancel))
    {
        QDialog::reject();
    }
    else if (button == ui->buttonBox->button(QDialogButtonBox::Ok))
    {
        QDialog::accept();
    }

}

opt_sim integrator::getIntInfo()
{
    opt_sim sim(false);

    sim.integrator = (IntegratorType)ui->cbIntegrator->currentData().toUInt();
    sim.order      = ui->leOrder->text().toDouble();
    sim.stepSize   = ui->leStepSize->text().toDouble();
    sim.absTol     = ui->leAbsTol->text().toDouble();
    sim.relTol     = ui->leRelTol->text().toDouble();

    return sim;
}

void integrator::setIntInfo(opt_sim sim)
{
    ui->leOrder->setText(QString::number(sim.order,NUM_FORMAT,NUM_DEC));
    ui->leStepSize->setText(QString::number(sim.stepSize,NUM_FORMAT,NUM_DEC));
    ui->leAbsTol->setText(QString::number(sim.absTol,NUM_FORMAT,NUM_DEC));
    ui->leRelTol->setText(QString::number(sim.relTol,NUM_FORMAT,NUM_DEC));
    ui->cbIntegrator->setCurrentIndex(getIntegratorIndex(sim.integrator));
}
