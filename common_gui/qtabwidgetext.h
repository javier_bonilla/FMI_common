/*
*    This file is part of simint
*
*    simint - Simulation Interface
*    Copyright (C) 2017 - Javier Bonilla
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef QTABWIDGETEXT_H
#define QTABWIDGETEXT_H

#include <QWidget>
#include <QString>
#include <QIcon>
#include <QTabWidget>

#include "resultwin.h"

class QTabWidgetExt : public QTabWidget
{
    Q_OBJECT

    public:
        QTabWidgetExt(QWidget *parent = 0);
        virtual ~QTabWidgetExt();

        int addTab(QWidget *widget, const QString &label);
        int addTab(QWidget *widget, const QIcon   &icon,  const QString &label);
        int addTab(QWidget *widget, const QString &label, QWidget *w, QIcon wi, QString wt);
        int addTab(QWidget *widget, const QIcon   &icon,  const QString &label, QWidget *w, QIcon wi, QString wt);

        int insertTab(int index, QWidget *widget, const QString &);
        int insertTab(int index, QWidget *widget, const QIcon& icon, const QString &label);
        int insertTab(int index, QWidget *widget, const QString &label, QWidget *w, QIcon wi, QString wt);
        int insertTab(int index, QWidget *widget, const QIcon& icon, const QString &label, QWidget *w, QIcon wi, QString wt);

        void hideWindows();

    private:
        QList<resultWin *>   rw;

        void insertButton(int index, QWidget *w, QIcon icon, QString text);

    private slots:
        void openTriggered();
};

#endif // QTABWIDGETEXT_H
