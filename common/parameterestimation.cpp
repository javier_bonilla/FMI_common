#include "parameterestimation.h"
//#include "common.h"
#include "parest.h"

const QString DAKOTA_THROW      = "throw";
const QString DAKOTA_FMU_PLUGIN = "plugin_fmu";
const QString DAKOTA_DIRECT     = "direct";
const QString DAKOTA_NONE       = "none";
const QString DAKOTA_FILE_OUT   = "optimization_out.txt";
const QString DAKOTA_FILE_ERROR = "optimization_error.txt";

namespace Dakota { extern PRPCache data_pairs; }

parameterEstimation::parameterEstimation(opt_project *p)
{
    // Set pointer to null
    prj = NULL;

    // Copy project
    copyProject(p,&prj);

    // Names for output and error files
    setFileOutError(fileOut,fileError);

    // Find log file
    QString file = findMOGALogFile(p);
    if (!file.trimmed().isEmpty())
        fileError = file;
}

parameterEstimation::~parameterEstimation()
{
    deleteProject(&prj);
}

int parameterEstimation::getTime()
{
    return t_pe;
}

opt_project *parameterEstimation::getProject()
{
    return prj;
}

QString parameterEstimation::getOutFile()
{
    return fileOut;
}

QString parameterEstimation::getErrorFile()
{
    return fileError;
}

QString parameterEstimation::getDakota_log_level() const
{
    return dakota_log_level;
}

void parameterEstimation::setDakota_log_level(const QString &value)
{
    dakota_log_level = value;
}

void parameterEstimation::requestStop()
{
    emit stop();

}

void parameterEstimation::update()
{   
    emit parEstUpdate(t_pe,0,0);
}

void parameterEstimation::run()
{
    // Timer
    QTime timer;

    // Start timer
    timer.start();

    // Clear previous Dakota evaluations
    //PRPCache().swap(data_pairs);
    //data_pairs.clear();
    //data_pairs.get<ordered>().clear();
    //data_pairs.get<hashed>().clear();

    //qDebug() << data_pairs.empty();

    // Dakota options
    Dakota::ProgramOptions opts;

    // Output and error file
    opts.output_file(fileOut.toStdString());
    opts.error_file(fileError.toStdString());

    // Delay validation/sync of the Dakota database and iterator
    // construction to allow update after all data is populated
    bool check_bcast_construct = false;

    // Dakota library
    Dakota::data_pairs.clear();
    Dakota::LibraryEnvironment env(MPI_COMM_WORLD,opts,check_bcast_construct);
    Dakota::data_pairs.clear();

    // Simulator interface
    SIM::FMUInterface *FMU_int = NULL;

    try{
        // Extract FMU file
        extractFMUdir(prj);

        // Define ProblemDB in Dakota
        buildDakotaProblemDB(prj,dakota_log_level,env);

        // Clear previous Dakota evaluations
        //PRPCache().swap(data_pairs);
        //data_pairs.clear();
        //data_pairs.get<ordered>().clear();
        //data_pairs.get<hashed>().clear();

        //qDebug() << data_pairs.empty();

        // FMU interface
        FMU_int = serial_interface_plugin(prj,env);

        // Connect signals/slots
        connect(FMU_int,SIGNAL(update()),this,SLOT(update()));
        connect(this,SIGNAL(stop()),FMU_int,SLOT(stop()));

        // Execute parameter estimation
        Dakota::data_pairs.clear();
        executeDakota(env);
        Dakota::data_pairs.clear();

        // Problem status
        unsigned status = FMU_int->wasStopped() ? DAKOTA_STOPPED : DAKOTA_OK;

        // Save results
        saveResults(&prj,env,env.problem_description_db());

        // Delete FMU dir
        deleteDir(prj->getModel().uri);

        // Optimization finished
        t_pe = timer.elapsed();

        // Emit signals
        emit finished(t_pe,prj->getExp(),prj->getPrb(),status);
        emit end();

    }catch(std::exception& e)
    {
        emit parEstError(DAKOTA_ERROR,e.what());
        emit end();
    }
}
