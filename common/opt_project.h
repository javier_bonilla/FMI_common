#ifndef OPT_PROJECT_H
#define OPT_PROJECT_H

#include <QtCore>

#include "import/integrators/include/IntegratorType.h"
#include "opt_problem.h"

//#include "ui_conf.h"

const int ptNoType     = -1;
const int ptSimulation = 0;
const int ptEstimation = 1;
const int ptInputEst   = 2;

// FMI version
const unsigned FMI_1_0 = 1;
const unsigned FMI_2_0 = 2;

// FMU file status
const unsigned FMU_VALID     = 1;
const unsigned FMU_ZIP_ERROR = 2;
const unsigned FMU_XML_ERROR = 3;
const unsigned FMU_NO_FILE   = 4;

// FMU simulation status
const unsigned SIM_FMU_VALID             = FMU_VALID;
const unsigned SIM_FMU_FILE_ZIP_ERROR    = FMU_ZIP_ERROR;
const unsigned SIM_FMU_FILE_XML_ERROR    = FMU_XML_ERROR;
const unsigned SIM_FMU_NO_FILE           = FMU_NO_FILE;
const unsigned SIM_FMU_INVALID_VERSION   = 5;
const unsigned SIM_FMU_LOADING_ERROR     = 6;
const unsigned SIM_FMU_SIMULATION_ERROR  = 7;
const unsigned SIM_FMU_TIME_TYPE_INVALID = 8;
const unsigned SIM_FMU_STOP              = 9;
const unsigned SIM_FMU_NONE              = 10;

// Variable type
const unsigned FMI_REAL      = 1;
const unsigned FMI_INTEGER   = 2;
const unsigned FMI_BOOL      = 3;
const unsigned FMI_STRING    = 4;
const unsigned FMI_UNDEFINED = 0;

const QString FMU_INPUT           = "input";
const QString FMU_OUTPUT          = "output";
const QString FMU_PARAMETER       = "parameter";
const QString FMU_CAUSALITY       = "causality";
const QString FMU_VARIABILITY     = "variability";
const QString FMU_MIN             = "min";
const QString FMU_DEC_TYPE        = "declaredType";
const QString FMU_MAX             = "max";
const QString FMU_UNIT            = "unit";
const QString FMU_QUANTITY        = "quantity";
const QString FMU_DISUNIT         = "displayUnit";
const QString FMU_TXT_REAL        = "Real";
const QString FMU_TXT_INT         = "Integer";
const QString FMU_TXT_BOOL        = "Boolean";
const QString FMU_TXT_STRING      = "String";
const QString FMU_GENERATION_TOOL = "generationTool";
const QString FMU_NAME            = "name";
const QString FMU_DESCRIPTION     = "description";
const QString FMU_DATE_TIME       = "generationDateAndTime";
const QString FMU_GUID            = "guid";
const QString FMU_START           = "start";
const QString FMU_MODEL_EXCHANGE  = "ModelExchange";
const QString FMU_COSIMULATION    = "CoSimulation";
const QString FMU_IMPLEMENTATION  = "Implementation";
const QString FMU_CALCULATED      = "calculatedParameter";
const QString FMU_LOCAL           = "local";
const QString FMU_INDEPENDENT     = "independent";
const QString FMU_CONSTANT        = "constant";
const QString FMU_FIXED           = "fixed";
const QString FMU_TUNABLE         = "tunable";
const QString FMU_DISCRETE        = "discrete";
const QString FMU_CONTINUOUS      = "continuous";
const QString FMU_VERSION         = "version";
const QString FMU_LICENSE         = "license";
const QString FMU_AUTHOR          = "author";
const QString FMU_COPYRIGHT       = "copyright";
const QString FMU_VARIABLENAMING  = "variableNamingConvention";
const QString FMU_DESC            = "description";
const QString FMU_NUMBEROFEVENTS  = "numberOfEvents";


// Model description file in FMU
const QString fModelDescription = "modelDescription.xml";

// Date format in FMU
const QString FMI_date_format = "yyyy-MM-ddThh:mm:ssZ";

// Type for time events
const unsigned TT_INPUT  = 1;
const unsigned TT_OUTPUT = 2;
const unsigned TT_IN_OUT = 3;

// Time values type
const unsigned ttTimeDist = 0;
const unsigned ttTimeFile = 1;
const unsigned ttTimeStep = 2;

// Type of input
const unsigned ttFree  = 0;
const unsigned ttFixed = 1;
const unsigned ttFile  = 2;
const unsigned ttList  = 3;

// Index undefined
const int INDEX_UNDEFINED = -1;

// Simulator settings
const QString stOrder    = "order";
const QString stStepSize = "stepsize";
const QString stAbsTol   = "abstol";
const QString stRelTol   = "reltol";
const QString stInt      = "integrator";

// Defaults simulator
const QString        DEFAULT_STEP_SIZE    = "1e-4";
const QString        DEFAULT_ORDER        = "4";
const QString        DEFAULT_ABS_TOL      = "1e-6";
const QString        DEFAULT_REL_TOL      = "1e-6";
const IntegratorType DEFAULT_INTEGRATOR   = bdf;
const double         DEFAULT_START_TIME   = 0;
const double         DEFAULT_STOP_TIME    = 1;
const int            DEFAULT_N_INTERVAL   = 100;
const int            DEFAULT_TYPE_TIME    = 0;
const unsigned       DEFAULT_REDUC_SAM    = 1;
const unsigned       DEFAULT_OUT_TIME     = 0;
const int            DEFAULT_OUT_INTERVAL = 1000;
const double         DEFAULT_STEP_OUT     = 1;

class opt_project;

class typeDefinition
{
    public:
        QString  name;
        unsigned type;
        QString  unit;
        QString  quantity;
        QString  displayUnit;
        QString  min;
        QString  max;
};

class variable
{
    public:
        QString  name;    // Name
        unsigned type;    // Type
        bool     isParam; // Is a parameter
        QString  desc;    // Description
        QString  value;   // Value
        QString  min;     // Min value
        QString  max;     // Max value
        int      typeDef; // Type def index

        // Optional info
        int      index;   // Index in file (for trajectory inputs)
        QString  cau;
        QString  var;
};

class opt_model
{
    private:
        // Model provided information
        QString FMUfileName;
        //QString work_dir;

        // Private methods
        void getModelInfo(QString path);
        void clearModelData();
        void checkModelExchangeCosimulation(QString filename);
        int  findTypeDef(QString name, unsigned type);

    public:

        // User data
        QStringList     p_name_new;
        QStringList     p_value_new;

        // Model generated information
        unsigned        valid;
        bool            modelExchanged;
        bool            cosimulation;
        unsigned        fmiVer;
        QString         uri;
        QString         modelName;
        QString         modelPath;
        QString         guid;
        QString         desc;
        QString         author;
        QString         version;
        QString         copyright;
        QString         license;
        QString         variableNaming;
        QString         numberOfEvents;

        bool            expData;
        double          expStart;
        double          expStop;
        double          expTolerance;
        double          expStepSize;

        QList<typeDefinition> types;

        QList<variable> params;
        QList<variable> inputs;
        QList<variable> outputs;
        QList<variable> structure;

        // Model additional generated information
        QString   vendor;
        QDateTime date;

        opt_model();

        QString getFMUfileName() const;
        void setFMUfileName(const QString &value, QString path);
        //QString getWork_dir() const;

        // Search inf
        int findParam(QString p);
        int findInput(QString i);
};

class opt_sim
{
public:

    // Model information
    IntegratorType integrator;
    double         stepSize;
    double         order;
    double         absTol;
    double         relTol;

    opt_sim(bool restoreDefault = true);
};

class time_sim
{
public:
    void            insert (double v, unsigned t);
    void            clear();
    double          getValue(unsigned i);
    unsigned        getType(unsigned i);
    int             indexOf(double t);
    unsigned        count();
    QList<double>   getValue();
    QList<unsigned> getType();
    void            setType(int i, unsigned t);
    unsigned        lowerBound(double v);

protected:
    QList<double> value;
    QList<unsigned> type;
};

class opt_exp
{
public:

    // Time
    double   startTime;
    double   stopTime;
    unsigned ReducSam;
    unsigned simStatus;

    // Inputs
    QString              fileName;
    unsigned             fileFormat;
    unsigned             timeInType;
    int                  nInInt;
    QString              timeVar;
    QStringList          model_inputs;
    QList<unsigned>      fmiType_inputs;
    QList<unsigned>      type_inputs;
    QStringList          file_inputs;
    QStringList          fixed_inputs;

    // Outputs
    unsigned             timeOutType;
    int                  nOutInt;
    double               stepOutTime;

    // For interfacing with FMI++ library
    unsigned buildData(QList<variable> inputs, opt_project *prj, QList<QList<double> > inp_traj = QList<QList<double> >()); //  = {{}} - Error in cluster Qt 5.5.1

    std::string *getiRealNames();
    std::string *getiIntNames();
    std::string *getiBoolNames();
    std::string *getiStringNames();

    unsigned getiRealCount();
    unsigned getiIntCount();
    unsigned getiBoolCount();
    unsigned getiStringCount();

    double      *getiRealVals(double time);
    int         *getiIntVals(double time);
    int         *getiBoolVals(double time);
    const char **getiStringVals(double time);

    // results
    QList<QList<double> > values_output;
    QList<QList<double> > values_input;
    QList<double>         time_output;

    time_sim timeIns;

    opt_exp();

private:
    std::string *getStringList(QStringList qList);

    // Experiment generated information
    QStringList     iRealName;
    QStringList     iIntName;
    QStringList     iStringName;
    QStringList     iBoolName;

    QList<QMap<double,double> >  input_real_values;
    QList<QMap<double,int> >     input_int_values;
    QList<QMap<double,bool> >    input_bool_values;
    QList<QMap<double,QString> > input_string_values;
};

class opt_project
{
    protected:
        // Project provided information
        int       type;      // Project type
        QString   name;      // Project info

        opt_model model;     // Model class
        opt_sim   sim;       // Simulation class
        opt_exp   exp;       // Experiment class

        opt_problem prb;     // Optimization problem class

        double          percentage; // Percentage in simulation/optimization
        double          perTime;    // Stop time in simulation/optimization
        QStringList     lmesg;      // Log message texts
        QList<unsigned> ltype;      // Log message types
        QStringList     ltime;      // Log message times

        // Project generated information
        bool      changed;   // Project status
        QString   filename;  // Project filename

    public:
        opt_project(int type);
        ~opt_project();

        // Methods to load and save
        bool load_from_file(QString filename);
        void save_to_file();

        // Set and get data
        QString getName() const;
        void setName(const QString &value);
        opt_model getModel() const;
        void setModel(const opt_model &value);
        opt_sim getSim() const;
        void setSim(const opt_sim &value);
        opt_exp getExp() const;
        void setExp(const opt_exp &value);
        bool getChanged() const;
        QString getFilename() const;
        void setFilename(const QString &value);
        int getType() const;
        void setType(int value);
        void setChanged(bool value);
        void setSimStatus(unsigned status);
        opt_problem getPrb() const;
        void setPrb(const opt_problem &value);
        double getPercentage() const;
        void setPercentage(double value);
        QStringList getLmesg() const;
        void setLmesg(const QStringList &value);
        QList<unsigned> getLtype() const;
        void setLtype(const QList<unsigned> &value);
        QStringList getLtime() const;
        void setLtime(const QStringList &value);
        double getPerTime() const;
        void setPerTime(double value);

        // Delete dynamic data share between projects
        void deletePrbMethodArgs();
};

// Procedures
opt_sim sim_restoreFromDefaults();

// Read and write data in file
QDataStream &operator<<(QDataStream &out, const opt_model &m);
QDataStream &operator>>(QDataStream &in, opt_model &m);

QDataStream &operator<<(QDataStream &out, const opt_project &p);
QDataStream &operator>>(QDataStream &in, opt_project &p);

QDataStream &operator<<(QDataStream &out, const opt_sim &s);
QDataStream &operator>>(QDataStream &in, opt_sim &s);

QDataStream &operator<<(QDataStream &out, const opt_exp &e);
QDataStream &operator>>(QDataStream &in, opt_exp &e);

#endif // OPT_PROJECT_H
