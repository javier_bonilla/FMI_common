#include "inputestimation.h"
#include "common.h"
#include "parest.h"

const QString DAKOTA_THROW      = "throw";
const QString DAKOTA_FMU_PLUGIN = "plugin_fmu";
const QString DAKOTA_DIRECT     = "direct";
const QString DAKOTA_NONE       = "none";
const QString DAKOTA_FILE_OUT   = "optimization_out.txt";
const QString DAKOTA_FILE_ERROR = "optimization_error.txt";

inputEstimation::inputEstimation(opt_project *p)
{
    // Set pointer to null
    prj = NULL;

    // Copy project
    copyProject(p,&prj);

    // Names for output and error files
    setFileOutError(fileOut,fileError);

    // Find log file
    QString file = findMOGALogFile(p);
    if (!file.trimmed().isEmpty()) fileError = file;
}

inputEstimation::~inputEstimation()
{
    deleteProject(&prj);
}

int inputEstimation::getTime()
{
    return t_pe;
}

opt_project *inputEstimation::getProject()
{
    return prj;
}

QString inputEstimation::getOutFile()
{
    return fileOut;
}

QString inputEstimation::getErrorFile()
{
    return fileError;
}

QString inputEstimation::getDakota_log_level() const
{
    return dakota_log_level;
}

void inputEstimation::setDakota_log_level(const QString &value)
{
    dakota_log_level = value;
}

void inputEstimation::requestStop()
{
    emit stop();
}

void inputEstimation::update()
{   
    emit inputEstUpdate(t_pe,0,0);
}

QList<double> TimeInstInputCount(opt_exp exp)
{
    QList<double> list;

    for(unsigned i=0;i<exp.timeIns.count();i++)
    {
        unsigned type = exp.timeIns.getType(i);

        if (((type == TT_INPUT) || (type == TT_IN_OUT)) &&
            (exp.timeIns.getValue(i)>=exp.startTime)    &&
            (exp.timeIns.getValue(i)<=exp.stopTime))
        list.append(exp.timeIns.getValue(i));
    }
    return list;
}

void inputEstimation::setInitialConditions(opt_model &mo, opt_exp exp)
{
    const QString levelStart = "levelstart";
    const QString levelSim   = "level_sim";

    int pos = findVar(levelSim,mo.outputs);

    if (pos>=0)
    {
        double newLevel = exp.values_output[pos].last();
        int    index    = mo.p_name_new.indexOf(levelStart);

        if (index<0)
        {
            mo.p_name_new.append(levelStart);
            mo.p_value_new.append(QString::number(newLevel));
        }
        else
            mo.p_value_new[index] = QString::number(newLevel);
        prj->setModel(mo);
    }
}

void writeInitialPopulation(QList<opt_result> pars)
{
    QFile file(FILE_INIT_POPULATION);
    if (file.open(QIODevice::WriteOnly))
    {
        QTextStream stream(&file);

        for(int i=0;i<pars.count();i++)
            stream << pars[i].getValues()[0] << ",";
        stream << endl;
    }
    file.close();
}

void inputEstimation::limitVariation(Dakota::LibraryEnvironment &env, QList<opt_result>pars)
{
    const double RANGE = 3;
    RealVector   varLowBound(pars.count());
    RealVector   varUppBound(pars.count());
    ModelList&   all_models = env.problem_description_db().model_list();
    Model&       model      = *all_models.begin();

    for(int i=0;i<pars.count();i++)
    {
        varLowBound(i) = pars[i].getValues()[0]-RANGE;//qMax(model.continuous_lower_bound(i),pars[i].getValues()[0]-RANGE);
        varUppBound(i) = pars[i].getValues()[0]+RANGE;//qMin(model.continuous_upper_bound(i),pars[i].getValues()[0]+RANGE);
    }

    model.continuous_lower_bounds(varLowBound);
    model.continuous_upper_bounds(varUppBound);
}

void inputEstimation::run()
{

    // Timer
    QTime timer;

    // Start timer
    timer.start();

    // Dakota options
    Dakota::ProgramOptions opts;

    // Output and error file
    opts.output_file(fileOut.toStdString());
    opts.error_file(fileError.toStdString());

    // Delay validation/sync of the Dakota database and iterator
    // construction to allow update after all data is populated
    bool check_bcast_construct = false;

    // Dakota library
    Dakota::LibraryEnvironment env(MPI_COMM_WORLD,opts,check_bcast_construct);

    // Simulator interface
    SIM::FMUInterface *FMU_int = NULL;

    try{
        // Extract FMU file
        extractFMUdir(prj);

        // Define ProblemDB in Dakota
        buildDakotaProblemDB(prj,dakota_log_level,env);

        // FMU interface
        FMU_int = serial_interface_plugin(prj,env);

        // Model
        opt_model mo_ori = prj->getModel();
        opt_model mo     = prj->getModel();

        // Experiment
        opt_exp   exp = prj->getExp();

        // Connect signals/slots
        connect(FMU_int,SIGNAL(update()),this,SLOT(update()));
        connect(this,SIGNAL(stop()),FMU_int,SLOT(stop()));

        // Status and iteration
        unsigned status = DAKOTA_OK;
        int      iter   = 0;

        // Whole simulation interval
        double iniTime = exp.startTime;
        double endTime = exp.stopTime;

        // Global results
        QList<opt_result> inputs;

        // Interval list
        QList<double> lint = TimeInstInputCount(exp);

        // Parameters, objectives and contratints
        QList<opt_result> pars;
        QList<opt_result> objs;
        QList<opt_result> cons;

        // Delete previous initial poupulation
        QFile::remove(FILE_INIT_POPULATION);

        // Loop for each time interval       
        while (status == DAKOTA_OK && iter < lint.count()-1)
        {

            // Set initial conditions for
            // each not initial iteration
            if (iter>0)
            {
                // Find best result to obtain
                // experiment for initial conditions
                // WARNING: configuration in GUI
                opt_exp exp_initial;

                if (FMU_int->getExp(pars,exp_initial))
                    setInitialConditions(mo,exp_initial);

                // Write best result for being part
                // of initial population
                // WARNING: configuration of file in GUI
                writeInitialPopulation(pars);

                // Limit variation of parameters
                // WARNING: configuration in GUI
                //limitVariation(env,pars);

            }

            // Set experiment time interval
            exp.startTime = lint[iter];
            exp.stopTime  = lint[iter+1];
            prj->setExp(exp);         

            // Delete previous store exps
            FMU_int->clearListExp();

            // Delete previous obj results for filtering
            FMU_int->clearPreobj();

            // Set project in FMU interface
            FMU_int->setProject(prj);

            // Execute parameter estimation
            executeDakota(env);

            // Experiment results
            exp = FMU_int->getProject()->getExp();

            // Problem status
            status = FMU_int->wasStopped() ? DAKOTA_STOPPED : DAKOTA_OK;

            // Delete previous results
            pars.clear();
            objs.clear();
            cons.clear();

            // Get results
            getResults(prj,env,env.problem_description_db(),pars,objs,cons);

            // Keep only best results
            for(int i=0; i<pars.count();i++)
            {
                double v = pars[i].getValues()[0];

                pars[i].clearValues();
                pars[i].setValue(v);

                //qDebug() << "Best param value = " << v;
            }

            // Store global results
            for(int i=0;i<pars.count();i++)
            {
                // If empty then copy
                if (inputs.empty())
                {
                    inputs.append(pars[i]);
                    // WARNING: first value is repeated for initialization reasons
                    inputs[0].setValue(pars[i].getValues()[0]);
                }
                // If not append new estimated values
                else
                {
                    QList<double> vals = inputs[i].getValues();
                    // WARNING: first value is repeated for initilization reasons
                    if (vals.isEmpty()) vals.append(pars[i].getValues()[0]);
                    vals.append(pars[i].getValues()[0]);
                    inputs[i].setValues(vals);
                }
            }

            // Next iteration
            iter++;
        }

        // Complete simulation or until last interval
        // before stop/error with estimated inputs
        exp.startTime = iniTime;
        exp.stopTime  = status == DAKOTA_OK ? endTime :lint[--iter];

        //qDebug() << "Iterations: " << iter;

        // Set estimated inputs
        QList<opt_parameter> opars = prj->getPrb().getParameters();

        // List a input values
        QList<QList<double> > inp_traj;

        // For each input
        for(int i=0;i<opars.count();i++)
        {
            // Find if it is already set in the experiment
            int pos = exp.model_inputs.indexOf(opars[i].getDescriptor());
            // Not found, add a new one
            if (pos<0)
            {
                exp.model_inputs.append(opars[i].getDescriptor());
                exp.type_inputs.append(ttList);
                exp.fmiType_inputs.append(FMI_REAL);
                exp.file_inputs.append(sEmpty);
                exp.fixed_inputs.append(sEmpty);
            }
            // Found, change type
            else
                exp.type_inputs[pos] = ttList;

            // Apply a discrete Infinite Impulse Response (IIR)
            // Low-Pass Filter (LPF) to reduce noise
            // WARNING: configure this in GUI
            //const double LPF_beta = 0.1; // [0,1]
            //for(int i=0;i<inputs.count();i++)
            //{
            //    QList<double> vals = inputs[i].getValues();
            //    for(int j=1;j<vals.count();j++)
            //        vals[j] = LPF_beta * vals[j] + (1-LPF_beta) * vals[j-1];
            //    inputs[i].setValues(vals);
            //}

            // Set values por input trajectory (in order)
            inp_traj.append(inputs[i].getValues());
        }

        // Build input data in project
        exp.buildData(prj->getModel().inputs,prj,inp_traj);

        // Set original model
        // original initial conditions
        prj->setModel(mo_ori);

        // Set project experiment
        prj->setExp(exp);

        // Simulation object. WARNING: no error control events
        simulation *sim = new simulation(prj);

        // Simulation - unpack FMU, do not build input data
        sim->run(true,false);

        // Get exp result and restore end time
        exp = sim->getProject()->getExp();
        exp.stopTime = endTime;

        // Save simulation results
        prj->setExp(exp);

        // Delete simulation object
        delete sim;

        // Delete FMU dir
        deleteDir(prj->getModel().uri);

        // Optimization finished
        t_pe = timer.elapsed();

        // Emit signals
        emit finished(t_pe,prj->getExp(),prj->getPrb(),status);
        emit end();

    }catch(std::exception& e)
    {
        emit inputEstError(DAKOTA_ERROR,e.what());
        emit end();
    }
}
