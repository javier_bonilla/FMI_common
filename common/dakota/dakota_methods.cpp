#include "dakota_methods.h"
#include "climits"
#include "warnings_off.h"
#include "warnings_on.h"

#include "DakotaInterface.hpp"
#include "DakotaModel.hpp"

#include "common.h"

using namespace Dakota;

void getParameterEstimationList(QList<opt_method> &lmethods)
{
    // ------------------------------------------------------------------------------------------------------------------------------------ //
    // MOGA METHOD                                                                                                                          //
    // ------------------------------------------------------------------------------------------------------------------------------------ //

    // Name, id and description.
    opt_method moga("Multi-objective Genetic Algorithm (MOGA)",Dakota::MOGA,
                    "MOGA stands for Multi-objective Genetic Algorithm, which is a global optimization method that does Pareto "
                    "optimization for multiple objectives. It supports general constraints and a mixture of real and discrete variables."
                    "<p><a href=\"https://dakota.sandia.gov//sites/default/files/docs/6.5/html-ref/method-moga.html\"><span style=\"text-decoration: underline; color:#2980b9;\">Dakota MOGA documentation</span></a></p>",
                    smFreeDer,sgGlobal,false,false,tcNonLinear,true);

    // Arguments
    QList<opt_method_arg *> moga_args;
    QList<opt_method_arg *> sa;
    QList<opt_method_arg *> la;

    // Fitness type argument
    moga_args.clear();
    la.clear();
    la.append(new opt_method_arg_keyword("Layer rank","layer_rank","Assign each member to a layer, based on domination the rank based on layers.",false));
    la.append(new opt_method_arg_keyword("Domination count","domination_count","Rank each member by the number of members that dominate it.",false));
    moga_args.append(new opt_method_arg_set("Fitness type","fitness_type","Select the fitness type for JEGA methods.",true,
                                            "domination_count","domination_count",la));

    // Replacement type
    la.clear();
    sa.clear();
    sa.append(new opt_method_arg_real("Below limit","below_limit","Limit number of designs dominating those kept.",false,6,6,0,1e300));
    sa.append(new opt_method_arg_real("Shrinkage fraction","shrinkage_fraction","Decrease the population size by a percentage.",true,0.9,0.9,0,1));
    la.append(new opt_method_arg_keyword("Elitist","elitist","Use the best designs to form a new population.",false));
    la.append(new opt_method_arg_keyword("Roulette wheel","roulette_wheel","Replace population.",false));
    la.append(new opt_method_arg_keyword("Unique roulette wheel","unique_roulette_wheel","Replace population.",false));
    la.append(new opt_method_arg_struct("Below limit","below_limit","Limit number of designs dominating those kept.",false,sa));
    moga_args.append(new opt_method_arg_set("Replacement type","replacement_type","Select a replacement type for JEGA methods.",true,
                                        "below_limit","below_limit",la));

    // Niching type
    //la.clear();
    //la.append(new opt_method_arg_keyword("Radial","radial","Set niching distance to percentage of non-dominated range.",false));
    //la.append(new opt_method_arg_keyword("Distance","distance","Enforce minimum Euclidean distance between designs.",false));
    //la.append(new opt_method_arg_keyword("Max designs","max_designs","Limit number of solutions to remain in the population.",false));
    //moga_args.append(new opt_method_arg_set("Niching type","niching_type","Specify the type of niching pressure.",true,
    //                                    "none","none",la));

    // Convergence type
    sa.clear();
    sa.append(new opt_method_arg_keyword("Metric tracker","metric_tracker","Track changes in the non-dominated frontier.",false));
    sa.append(new opt_method_arg_real("Percent change","percent_change","Define the convergence criterion for JEGA methods.",true,0.1,0.1,0,1e300));
    sa.append(new opt_method_arg_integer("Num generations","num_generations","Define the convergence criterion for JEGA methods.",true,10,10,0,INT_MAX));
    moga_args.append(new opt_method_arg_struct("Convergence type","convergence_type","Select the convergence type for JEGA methods.",true,sa));

    // Postprocessor type
    sa.clear();
    sa.append(new opt_method_arg_keyword("Orthogonal distance","orthogonal_distance	","Get subset of Pareto front based on distance.",false));
    moga_args.append(new opt_method_arg_struct("Postprocessor type","postprocessor_type","Post process the final solution from MOGA.",true,sa));

    // Max iterations
    moga_args.append(new opt_method_arg_integer("Max iterations","max_iterations","Number of iterations allowed for optimizers and adaptive UQ methods.",true,
                                            100,100,1,INT_MAX));

    // Max function evaluations
    moga_args.append(new opt_method_arg_integer("Max function evaluations","max_function_evaluations","Number of function evaluations allowed for optimizers.",true,
                                            1000,1000,1,INT_MAX));

    // Population size
    moga_args.append(new opt_method_arg_integer("Population size","population_size","Set the initial population size in JEGA methods.",true,
                                            50,50,1,INT_MAX));

    // Log file
    moga_args.append(new opt_method_arg_filename("Log file","log_file","Specify the name of a log file",true,"JEGAGlobal.log","JEGAGlobal.log",false));

    // Print each population
    moga_args.append(new opt_method_arg_keyword("Print each population","print_each_pop","Print every population to a population file.",true));

    // Initialization type
    la.clear();
    la.append(new opt_method_arg_keyword("Simple random","simple_random","Create random initial solutions.",false));
    la.append(new opt_method_arg_keyword("Unique random","unique_random","Create random initial solutions, but enforce uniqueness.",false));
    la.append(new opt_method_arg_filename("Flat file","flat_file","Read initial solutions from file",false,"","",true));
    moga_args.append(new opt_method_arg_set("Initialization type","initialization_type","Specify how to initialize the population.",true,
                                        "unique_random","unique_random",la));

    // Crossover type
    sa.clear();
    la.clear();
    la.append(new opt_method_arg_integer("Multi point binary","multi_point_binary","Use bit switching for crossover events.",false,0,0,0,INT_MAX));
    la.append(new opt_method_arg_integer("Multi point parametrized binary","multi_point_parameterized_binary","Use bit switching to crossover each design variable.",false,0,0,0,INT_MAX));
    la.append(new opt_method_arg_integer("Multi point real","multi_point_real","Perform crossover in real valued genome.",false,0,0,0,INT_MAX));
    sa.append(new opt_method_arg_integer("Num parents","num_parents","Number of parents in random shuffle crossover.",false,2,2,0,INT_MAX));
    sa.append(new opt_method_arg_integer("Num offspring","num_offspring","Number of offspring in random shuffle crossover.",false,2,2,0,INT_MAX));
    la.append(new opt_method_arg_struct("Suffle random","shuffle_random","Perform crossover by choosing design variable(s).",false,sa));
    sa.clear();
    sa.append(new opt_method_arg_set("Crossover type","crossover_type","Select a crossover type for JEGA methods.",true,"shuffle_random","shuffle_random",la));
    sa.append(new opt_method_arg_real("Crossover rate","crossover_rate","Specify the probability of a crossover event.",true,0.8,0.8,0,1));
    moga_args.append(new opt_method_arg_struct("Crossover type","crossover_type","Select a crossover type for JEGA methods.",true,sa));

    // Mutation type
    la.clear();
    la.append(new opt_method_arg_keyword("Bit random","bit_random","Mutate by flipping a random bit.",false));
    la.append(new opt_method_arg_keyword("Replace uniform","replace_uniform","Use uniformly distributed value over range of parameter.",false));
    sa.clear();
    sa.append(new opt_method_arg_real("Mutation scale","mutation_scale","Scales mutation across range of parameter",true,0.15,0.15,0,1));
    la.append(new opt_method_arg_struct("Offset normal","offset_normal","Set mutation offset to use a normal distribution.",false,sa));
    sa.clear();
    sa.append(new opt_method_arg_real("Mutation scale","mutation_scale","Scales mutation across range of parameter",true,0.15,0.15,0,1));
    la.append(new opt_method_arg_struct("Offset cauchy","offset_cauchy","Use a Cauchy distribution for the mutation offset.",false,sa));
    sa.clear();
    sa.append(new opt_method_arg_real("Mutation scale","mutation_scale","Scales mutation across range of parameter",true,0.15,0.15,0,1));
    la.append(new opt_method_arg_struct("Offset uniform","offset_uniform","Set mutation offset to use a uniform distribution.",false,sa));
    sa.clear();
    sa.append(new opt_method_arg_set("Mutation type","mutation_type","Select a mutation type for JEGA methods.",true,"replace_uniform","replace_uniform",la));
    sa.append(new opt_method_arg_real("Mutation rate","mutation_rate","Set probability of a mutation.",true,0.08,0.08,0,1));
    moga_args.append(new opt_method_arg_struct("Mutation type","mutation_type","Select a mutation type for JEGA methods.",true,sa));

    // Seed
    moga_args.append(new opt_method_arg_integer("Seed","seed","Seed of the random number generator. If not specified, the seed is randomly generated.",true,
                                            1,1,1,INT_MAX));

    // Convergence tolerance
    moga_args.append(new opt_method_arg_real("Convergence tolerance","convergence_tolerance","Stopping criterion based on convergence of the objective function or statistics.",true,
                                            1e-4,1e-4,0,1e300));

    // ******************************************************** //
    // WARNING: niching type -- depend on objective functions!! //
    // ******************************************************** //

    // *************************************************** //
    // WARNING: model_pointer -- block, surrogate models!! //
    // *************************************************** //

    // Set arguments in MOGA
    moga.setArgs(moga_args);

    // Append MOGA to methods
    lmethods.clear();
    lmethods.append(moga);

    // ------------------------------------------------------------------------------------------------------------------------------------ //
}
