#include "simulation.h"

#include <import/base/include/CallbackFunctions.h>
#include "sundials/sundials_config.h"
#include "boost/version.hpp"

#ifndef Q_OS_MAC
    #include <omp.h>
#endif

const int TIME_UPDATE_MS = 1500;
const int MAX_INT_SAME = 50;

QString logtext;

simulation::simulation()
{
    // No project
    prj = NULL;

    // Status
    status = fmiOK;
}

simulation::simulation(opt_project *p)
{
    // Copy project
    prj  = new opt_project(ptNoType);
    *prj = *p;

    // Status
    status = fmiOK;
}

simulation::~simulation()
{
    delete prj;
}

QString getLogText()
{
    return logtext;
}

void clearLogText()
{
    logtext.clear();
}

int simulation::getSimTime()
{
    return t_ms;
}

void simulation::stopSim()
{
    stop = true;
}

QString statusToString(fmi2Status status)
{
    switch(status)
    {
        case fmi2OK:      return "Ok";
        case fmi2Warning: return "Warning";
        case fmi2Discard: return "Discard";
        case fmi2Error:   return "Error";
        case fmi2Fatal:   return "Fatal";
        case fmi2Pending: return "Pending";
        default:          return "";
    }
}

void customLogger( fmi2ComponentEnvironment c, fmi2String instanceName, fmi2Status status, fmi2String category, fmi2String message, ... )
// Copied from callback::verboseLogger
{
    Q_UNUSED(c);
    Q_UNUSED(status);

    QString text = "";
    QString ins = instanceName;
    QString cat = category;
    QString msg = message;

    text += "<b>Instance:</b> <font color=\"blue\">" + ins    + "</font><br>";
    if (category!=sEmpty)  text   += "<b>Category:</b> <font color=\"green\">" + cat  + "</font><br>";
    text += "<b>Status:</b> <font color=\"red\">"   + statusToString(status) + "</font><br>";
    text += "<b>Message:</b> "  + msg    + "<br>" + "<br>";

    logtext += text;
}

void simulation::error(unsigned ERROR_CODE, opt_exp e)
{
    QString cadError = "Error loading FMU file";

    deleteDir(prj->getModel().uri);
    prj->setSimStatus(ERROR_CODE);
    switch (ERROR_CODE)
    {
        case SIM_FMU_LOADING_ERROR:
        {
            cadError = "Error loading FMU. Are the parameter and input values valid? Is the FMU valid for this operating system?";
            break;
        }
        case SIM_FMU_SIMULATION_ERROR:
        {
            cadError = "Error simulating FMU.";
            break;
        }
    }
    emit simError(ERROR_CODE,cadError);
    emit finished(0,e,prj->getPrb(),0);
    emit end();
}

bool correctStatus(fmiStatus s)
{
    return s == fmiOK || s == fmiWarning;
}

void infoToLog()
{
    logtext += "<h3><b>System information</b></h3>";
    logtext += "<b>Hostname:</b> "         + QSysInfo::machineHostName()          + "<br>";
    logtext += "<b>App architecture:</b> " + QSysInfo::buildCpuArchitecture()     + "<br>";
    logtext += "<b>CPU architecture:</b> " + QSysInfo::currentCpuArchitecture()   + "<br>";
#ifndef Q_OS_MAC
    logtext += "<b>Number of cores:</b> "  + QString::number(omp_get_num_procs()) + "<br>";
#endif
    logtext += "<b>Kernel type:</b> "      + QSysInfo::kernelType()                      + "<br>";
    logtext += "<b>Kernel version:</b> "   + QSysInfo::kernelVersion()                   + "<br>";
    logtext += "<b>Build Abi:</b> "        + QSysInfo::buildAbi()                        + "<br>";
    logtext += "<b>Operating system:</b> " + QSysInfo::prettyProductName()               + "<br>";
    logtext += "<b>OS type:</b> "          + QSysInfo::productType()                     + "<br>";
    logtext += "<b>OS version:</b> "       + QSysInfo::productVersion()                  + "<br>";
    logtext += "<b>Qt:</b> "               + QString(qVersion())                         + "<br>";
    logtext += "<b>Sundials:</b> "         + QString(SUNDIALS_PACKAGE_VERSION)           + "<br>";
    logtext += "<b>Boost (Odeint):</b> "   + QString(BOOST_LIB_VERSION).replace("_",".") + "<br>";
    logtext += "<br>";
}

void timeToLog(int t_ms)
{
    logtext += "<b>Simulation time:</b> " + msToTime(t_ms) + "<br><br>";
}

void simulation::run(bool unpackFMU, bool buildData)
{
    // Initialization flag
    bool initialized = false;

    // Timer
    QTime timer;

    // Stop
    stop = false;

    // Start timer
    t_ms = 0;
    timer.start();

    // Init random number generator
    qsrand(QDateTime::currentMSecsSinceEpoch()/1000);

    // Default configuration
    QString     instanceName    = "instance-" + QString::number(qrand());
    fmi2Boolean loggingOn       = fmi2False;
    fmi2Boolean stopBeforeEvent = fmi2False;
    fmi2Real    eventSearchPre  = 1e-4;

    // Info for model, integrator, experiment
    opt_model mo  = prj->getModel();
    opt_sim   sim = prj->getSim();
    opt_exp   exp = prj->getExp();
    Integrator::Properties int_pro;

    // Check FMU model is valid
    unsigned modelStatus = mo.valid;
    if (modelStatus != FMU_VALID){error(modelStatus,exp); return;}

    // Integrator properties
    int_pro.type   = sim.integrator;
    int_pro.order  = sim.order;
    int_pro.abstol = sim.absTol;
    int_pro.reltol = sim.relTol;

    // Extract FMU file
    if (unpackFMU)
    {
        if (extractFMUdir(prj).count() <=0)
        {
            prj->setSimStatus(SIM_FMU_FILE_ZIP_ERROR);
            exit(0);
        }
        mo = prj->getModel();
    }

    try{              
        // FMU variable
        fmi_2_0::FMUModelExchange fmu(QUrl::fromLocalFile(mo.uri).toString().toStdString().c_str(),
                                      mo.modelName.toStdString().c_str(),loggingOn,
                                      stopBeforeEvent,eventSearchPre,sim.integrator);

        // Status       
        if (fmu.getLastStatus() != fmiOK){error(SIM_FMU_LOADING_ERROR,exp); return;}

        // Instantiate
        status = fmu.instantiate(instanceName.toStdString());
        if (!correctStatus(status)){error(SIM_FMU_LOADING_ERROR,exp); return;}

        // Call backs for logging and memory
        //status = fmu.setCallbacks(callback2::verboseLogger,callback2::allocateMemory,callback2::freeMemory);
        status = fmu.setCallbacks(customLogger,callback2::allocateMemory,callback2::freeMemory);
        if (!correctStatus(status)){error(SIM_FMU_LOADING_ERROR,exp); return;}

        // Set integrator properties
        fmu.setIntegratorProperties(int_pro);
        if (fmu.getLastStatus() != fmiOK){error(SIM_FMU_LOADING_ERROR,exp); return;}

        // Set model parameters
        setModelParameters(fmu,prj->getModel());

        // Build input data for interfacing with FMI++
        if (buildData)
        {
            exp.buildData(prj->getModel().inputs,prj);
        }
        // otherwise delete previous calculated data in exp
        else
        {
            exp.values_input.clear();
            exp.values_output.clear();
            exp.time_output.clear();
        }
        // Save experiment in project
        prj->setExp(exp);

        // Integration loop
        time                = exp.startTime; // Initial time
        fmiTime   nextTime  = exp.startTime; // Next time
        int       counter   = 0;             // Counter foir integrating again the same interval
        unsigned  timeEvPos;                 // First input event >= exp.startTime
        bool      timeEvent;                 // Time event for inputs or outputs
        bool      set_in_out = false;        // Detect input & output time instants

        // Calculate timePos
        int index = exp.timeIns.indexOf(exp.startTime);
        timeEvPos = index>=0 ? index : exp.timeIns.lowerBound(exp.startTime);
        timeEvent = index>=0 ? true  : false;

        // Set FMU initial time
        fmu.setTime(time);

        // Main loop
        while ((time<exp.stopTime || (time==exp.stopTime && set_in_out)) &&
               correctStatus(status) && !stop && counter < MAX_INT_SAME)
        {           
            // Set model inputs
            if (timeEvent && (exp.timeIns.getType(timeEvPos) == TT_INPUT ||
                              exp.timeIns.getType(timeEvPos) == TT_IN_OUT))
            {
                setModelInputs(fmu,mo,exp,time);
            }

            // Initialization for the first time after setting inputs
            if (!initialized)
            {
                initialized = true;
                status      = fmu.initialize(true,sim.absTol);
                if (!correctStatus(status)){error(SIM_FMU_LOADING_ERROR,exp); return;}
            }

            // Handle events
            fmu.raiseEvent();
            fmu.handleEvents();

            // Integrate to nextTime
            time = fmu.integrate(nextTime);

            // Get status
            status = fmu.getLastStatus();

            // If integration is fine
            if (correctStatus(status))
            {
                // nextTime reached in the integration
                if (time>=nextTime)
                {
                    // If time is an input & output time instant then
                    // it is necessary to set inputs before store outputs
                    int  id_in_out = exp.timeIns.indexOf(time);
                    bool is_in_out = id_in_out>=0 &&
                                     exp.timeIns.getType(id_in_out) == TT_IN_OUT;

                    // But only for the first time
                    set_in_out = is_in_out && !set_in_out;

                    // Set model outputs
                    if (!set_in_out && timeEvent &&
                        (exp.timeIns.getType(timeEvPos) == TT_OUTPUT ||
                        exp.timeIns.getType(timeEvPos) == TT_IN_OUT))
                    {
                        setModelOutputs(&fmu,mo,exp,time);
                        prj->setExp(exp);
                    }
                }
                // nextTime did not reach, intermediate integration step
                else
                    timeEvent = false;

                // Next time step
                if (set_in_out)
                {
                    timeEvPos = exp.timeIns.indexOf(time);
                    timeEvent = true;
                    nextTime  = time;
                    counter   = 0;
                }
                else if (time >= nextTime)
                {
                    timeEvPos += exp.timeIns.count()>timeEvPos ? 1 : 0;
                    timeEvent  = exp.timeIns.count()>timeEvPos &&
                                 exp.timeIns.getValue(timeEvPos)<=exp.stopTime;
                    nextTime   = timeEvent ? exp.timeIns.getValue(timeEvPos) : exp.stopTime;
                    counter    = 0;
                }
                else
                {
                    // Counter for trying to integrate in the same interval
                    counter++;
                }
            }

            // Emit update, limit per time
            if (timer.elapsed() > t_ms + TIME_UPDATE_MS)
            {
                emit simUpdate(time,exp.startTime,exp.stopTime,timer.elapsed(),exp);
                t_ms = timer.elapsed();
            }
        }
        // Emit generic error
        if (!correctStatus(status)){error(SIM_FMU_SIMULATION_ERROR,exp); return;}

        // Emit error max number of tries
        if (counter >= MAX_INT_SAME)
        {
            QString msg = "Error when integrating the interval [" +
                          QString::number(time) + "," + QString::number(nextTime) + "]";

            prj->setSimStatus(SIM_FMU_SIMULATION_ERROR);
            emit simError(prj->getExp().simStatus,msg);
        }
        // Update event
        emit simUpdate(time,exp.startTime,exp.stopTime,timer.elapsed(),exp);
    }catch(char const* msg)
    {
        // Emit error
        prj->setSimStatus(SIM_FMU_SIMULATION_ERROR);
        emit simError(prj->getExp().simStatus,msg);
    }

    // Delete extracted FMU files
    if (unpackFMU) deleteDir(mo.uri);

    // Simulation finished
    t_ms = timer.elapsed();

    // Send time to log
    timeToLog(t_ms);

    // Set simulation status
    prj->setSimStatus(stop ? SIM_FMU_STOP : status == fmiOK ? SIM_FMU_VALID : SIM_FMU_SIMULATION_ERROR);

    // Emit final signals and return
    emit finished(t_ms,prj->getExp(),prj->getPrb(),0);
    emit end();
    return;
}

opt_project *simulation::getProject()
{
    return prj;
}

void simulation::setProject(opt_project *p)
{
    delete prj;

    prj = new opt_project(ptNoType);
    *prj = *p;
}

unsigned simulation::getProjectType()
{
    return prj->getType();
}

void setListVariablesFromFMU(fmi_2_0::FMUModelExchange *fmu, QList<variable> list, QList<QList<double>> &values)
{
    int i_reals = 0;

    for(int i=0;i<list.count();i++)
    {
        // only real supported right now
        if (list[i].type == FMI_REAL)
        {
            fmiReal value;

            value = fmu->getRealValue(list[i].name.toStdString());
            if (values.count()>i_reals)
                values[i_reals].append(value);
            else
            {
                QList<double> lval;

                lval.append(value);
                values.append(lval);
            }
            i_reals++;
        }
    }
}

void setListVariablesInputs(opt_exp &exp, fmi2Real time)
{
    if (exp.getiRealCount()>0)
    {
        fmi2Real *pd;

        pd = exp.getiRealVals(time);
        for(unsigned i=0;i<exp.getiRealCount();i++)
        {
            if (exp.values_input.count()>(int)i)
                exp.values_input[i].append(pd[i]);
            else
            {
                QList<double> lval;

                lval.append(pd[i]);
                exp.values_input.append(lval);
            }
        }
        delete[] pd;
    }
}

fmiStatus simulation::getStatus() const
{
    return status;
}

void simulation::setModelOutputs(fmi_2_0::FMUModelExchange *fmu, opt_model mo, opt_exp &exp, fmi2Real time)
{
    // Add time value for outputs
    exp.time_output.append(time);

    // Inputs loop -- When 0 values are obtained
    setListVariablesInputs(exp,time);

    //qDebug() << "Time: " << time;

    // Outputs loop
    setListVariablesFromFMU(fmu,mo.outputs,exp.values_output);
}
