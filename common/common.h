#ifndef COMMON_H
#define COMMON_H

#include<QString>
#include<QList>
#include<exception>

#include "opt_project.h"
#include "import/base/include/FMUModelExchange_v2.h"

// Words, characters and symbols
const QString t_space = " ";

// Decimal separator
const QString DECIMAL_SEP = ".";

// Strings
const QString sEmpty = "";
const QString m_startingParEst = "Starting parameter estimation";
const QString m_parEstFinished = "Parameter estimation finished";
const QString m_executionTime  = "Execution time:";
const QString m_error          = "Error";
const QString mOpenFile           = "Open file";
const QString mErrorFileVars      = "Error reading file variables";
const QString mErrorFileData      = "Error reading file data";


const QString tNumInt        = "Numerical integrator:";
const QString tNumInt2       = "Numerical Integrator";
const QString tNumIntConf    = "Numerical integrator configuration";
const QString tOrder         = "Order:";
const QString tAbsTol        = "Absolute tolerance:";
const QString tRelTol        = "Relative tolerance:";
const QString tSetDef        = "Set as default";
const QString tRestoreDef    = "Restore default";
const QString tStepSize      = "Step size:";
const QString tDesc2         = "Description:";
const QString tOutput        = "Output";
const QString tOutputs       = "Outputs";
const QString tDesc          = "Description";
const QString tQuantityUnit  = "Quantity [unit]";
const QString tName          = "Name";
const QString tCau           = "Cau";
const QString tVar           = "Var";
const QString tValue         = "Value";
const QString tMin           = "Min";
const QString tMax           = "Max";
const QString tGraphStyle    = "Graph style";
const QString tLineSeries    = "Line series";
const QString tGraphs        = "Graphs";
const QString tReal          = "Real";
const QString tInteger       = "Integer";
const QString tBool          = "Bool";
const QString tString        = "String";
const QString tUndefined     = "Undefined";
const QString tCalculated    = "Calculated";
const QString tParam         = "Parameter";
const QString tInput         = "Input";
const QString tFile          = "File";
const QString tLocal         = "Local";
const QString tIndependent   = "Independent";
const QString tConstant      = "Constant";
const QString tFixed         = "Fixed";
const QString tTunable       = "Tunable";
const QString tDiscrete      = "Discrete";
const QString tContinuous    = "Continuous";
const QString tCSV           = "Comma-separated values (CSV)";
const QString tData          = "Data";

// File status
const unsigned FILE_OK           = 1;
const unsigned FILE_NO_EXIST     = 2;
const unsigned FILE_UNSUPPORTED  = 3;
const unsigned FILE_WRONG_FORMAT = 4;
const unsigned FILE_NO_VAR       = 5;

// Input data or file types
const unsigned int ffCSV = 1;
const unsigned int ffTRJ = 2;
const unsigned int ffDAT = 3;

// File extensions
const QString eMAT  = "mat";
const QString eCSV  = "csv";
const QString eTXT  = "txt";

// Log message type
const unsigned LOG_INFO    = 1;
const unsigned LOG_WARNING = 3;
const unsigned LOG_ERROR   = 4;

// Constraint symbols
const QString sLessEqual = QString(0x2264);
const QString sEqual     = "=";

// Dakota log level
const QString DAKOTA_SILENT  = "SILENT";
const QString DAKOTA_QUIET   = "QUIET";
const QString DAKOTA_NORMAL  = "NORMAL";
const QString DAKOTA_VERBOSE = "VERBOSE";
const QString DAKOTA_DEBUG   = "DEBUG";

// Search variable name
int searchVar(QList<variable> vars, QString name);


// Get data from files
template <typename T>
unsigned getDataFromFile(QString file, unsigned format, QStringList names, QList<QList<T> > &values, QString &error);
template <typename T>
unsigned getDataCSVfile(QString file, QStringList names, QList<QList<T> > &values, QString &error);
template <typename T>
unsigned getDataMATfile(QString file, QStringList names, QList<QList<T> > &values, QString &error);

// Check file extension
unsigned checkFileExistAndSupported(QString file);
unsigned checkFileExistAndSupported(QString file, QString &ext);

// Project path management
QDir    projectQDir(opt_project *prj);
QString projectPath(opt_project *prj);
QString fileRelativeToProject(opt_project *prj, QString filename);
QString fileAbsoluteFromProject(opt_project *prj, QString filename);

// Simulator configuration - parameters and inputs
void setModelParameters(fmi_2_0::FMUModelExchange &fmu, opt_model mo);
void setModelInputs(fmi_2_0::FMUModelExchange &fmu, opt_model mo, opt_exp exp, fmi2Real time);

// Milliseconds to second
QString msToTime(int ms);

// Send message to log
void sendToLog(QStringList &log, QList<unsigned> &ltype, QStringList &ltime,
               QString l, unsigned type = LOG_INFO, QString time = sEmpty);

// Find item in equality/inequality constraint
int findSelItem(opt_const_eq l);
int findSelItem(opt_const_ie l);

// Formatted names for objective functions, equality and inequality constraints
std::string objFormatedName(opt_objfnc obj);
std::string ceqFormatedName(opt_const_eq c, QList<variable> outs);
std::string cieFormatedName(opt_const_ie c, QList<variable> outs);

// Extract FMU zip file
QStringList extractFMUdir(opt_project *prj);
void        deleteDir(QString dir);

// Find MOGA log filename
QString findMOGALogFile(opt_project *prj);

// Find variable by name
int findVar(QString v, QList<variable> list);

// Copy/delete method arguments
void deleteArgs(QList<opt_method_arg *> args);
void copyArgs(QList<opt_method_arg *> &des, const QList<opt_method_arg *> src);

// List of QImages to video
bool imagesToVideo(QList<QImage> li, QString file, int framesPerSecond);

// WARNING: configure in GUI
const QString FILE_INIT_POPULATION = "init_population";

// Windows id for settings
const QString intWin    = "intWindow";
const QString outWin    = "outWindow";
const QString treeOut   = "treeOut";

#endif // COMMON_H
