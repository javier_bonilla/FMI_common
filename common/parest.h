#ifndef PAREST_H
#define PAREST_H

#include <QString>

#include "dakota/fmuinterface.h"
#include "opt_project.h"

const unsigned DAKOTA_ERROR   = 0;
const unsigned DAKOTA_OK      = 1;
const unsigned DAKOTA_STOPPED = 2;

// Helper functions
void copyProject(opt_project *source, opt_project **dest);
void setFileOutError(QString &fileOut, QString &fileError);
void getResults(opt_project *prj, LibraryEnvironment env, Dakota::ProblemDescDB &pdb,QList<opt_result> &pars, QList<opt_result> &objs, QList<opt_result> &cons);
void saveResults(opt_project **prj, LibraryEnvironment env, ProblemDescDB &pdb);
void argMathcing(QString name, QVariant value, Dakota::DataMethodRep *dmer);
void argKeyword(opt_method_arg_keyword *arg, Dakota::DataMethodRep *dmer);
void argReal(opt_method_arg_real *arg, Dakota::DataMethodRep *dmer);
void argInt(opt_method_arg_integer *arg, Dakota::DataMethodRep *dmer);
void argFile(opt_method_arg_filename *arg, Dakota::DataMethodRep *dmer);
void argSubSet(opt_method_arg_set *arg, Dakota::DataMethodRep *dmer);
void argStruct(opt_method_arg_struct *arg, Dakota::DataMethodRep *dmer);
void setDakotaMethodArgs(QList<opt_method_arg *> args, Dakota::DataMethodRep *dmer);
void executeDakota(LibraryEnvironment &env);
SIM::FMUInterface *serial_interface_plugin(opt_project *prj, LibraryEnvironment &env);
//SIM::FMUInterface *parallel_interface_plugin(Dakota::LibraryEnvironment& env);

Dakota::DataMethod    setDakotaMethod(opt_project *prj, QString dakota_log_level);
Dakota::DataModel     setDakotaModel();
Dakota::DataVariables setDakotaVariables(opt_project *prj);
Dakota::DataInterface setDakotaInterface();
Dakota::DataResponses setDakotaResponses(opt_project *prj);

void deleteProject(opt_project **prj);
void deleteDakotaProblemDB(Dakota::LibraryEnvironment **env);
void deleteFMUInterface(SIM::FMUInterface **FMU_int);

void buildDakotaProblemDB(opt_project *prj, QString dakota_log_level, LibraryEnvironment &env);

// Perform parameter estimation
unsigned runParEst(opt_project *p, QString dakota_log_level);

#endif // PAREST_H
